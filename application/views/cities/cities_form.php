<?php $this->load->view('templates/header');?>
<div class="row" style="margin-bottom: 20px">
            <div class="col-md-4">
                <h2>Cities <?php echo $button ?></h2>
            </div>
            <div class="col-md-8 text-center">
                <div id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
        </div>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Province Id <?php echo form_error('province_id') ?></label>
            <input type="text" class="form-control" name="province_id" id="province_id" placeholder="Province Id" value="<?php echo $province_id; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Province <?php echo form_error('province') ?></label>
            <input type="text" class="form-control" name="province" id="province" placeholder="Province" value="<?php echo $province; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Type <?php echo form_error('type') ?></label>
            <input type="text" class="form-control" name="type" id="type" placeholder="Type" value="<?php echo $type; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Name <?php echo form_error('name') ?></label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Postal Code <?php echo form_error('postal_code') ?></label>
            <input type="text" class="form-control" name="postal_code" id="postal_code" placeholder="Postal Code" value="<?php echo $postal_code; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Created Datetime <?php echo form_error('created_datetime') ?></label>
            <input type="text" class="form-control" name="created_datetime" id="created_datetime" placeholder="Created Datetime" value="<?php echo $created_datetime; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Updated Datetime <?php echo form_error('updated_datetime') ?></label>
            <input type="text" class="form-control" name="updated_datetime" id="updated_datetime" placeholder="Updated Datetime" value="<?php echo $updated_datetime; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Created By <?php echo form_error('created_by') ?></label>
            <input type="text" class="form-control" name="created_by" id="created_by" placeholder="Created By" value="<?php echo $created_by; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Updated By <?php echo form_error('updated_by') ?></label>
            <input type="text" class="form-control" name="updated_by" id="updated_by" placeholder="Updated By" value="<?php echo $updated_by; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('cities') ?>" class="btn btn-default">Cancel</a>
	</form><?php $this->load->view('templates/footer');?>