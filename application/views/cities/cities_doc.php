<!doctype html>
<html>
    <head>
        <title>SOCIANOVATION - Web Administration</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Cities List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Province Id</th>
		<th>Province</th>
		<th>Type</th>
		<th>Name</th>
		<th>Postal Code</th>
		<th>Created Datetime</th>
		<th>Updated Datetime</th>
		<th>Created By</th>
		<th>Updated By</th>
		
            </tr><?php
            foreach ($cities_data as $cities)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $cities->province_id ?></td>
		      <td><?php echo $cities->province ?></td>
		      <td><?php echo $cities->type ?></td>
		      <td><?php echo $cities->name ?></td>
		      <td><?php echo $cities->postal_code ?></td>
		      <td><?php echo $cities->created_datetime ?></td>
		      <td><?php echo $cities->updated_datetime ?></td>
		      <td><?php echo $cities->created_by ?></td>
		      <td><?php echo $cities->updated_by ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>