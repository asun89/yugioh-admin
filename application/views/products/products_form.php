<?php $this->load->view('templates/header');?>
<!-- CSS file -->
<link rel="stylesheet" href="<?php echo base_url('assets/vendor/easyautocomplete/easy-autocomplete.min.css') ?>">

<div class="row" style="margin-bottom: 20px">
            <div class="col-md-4">
                <h2>Products <?php echo $button ?></h2>
            </div>
            <div class="col-md-8 text-center">
                <div id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
        </div>
        <form action="<?php echo $action; ?>" method="post">
        <div class="form-group">
            <label for="int">Search Card by SKU and Rarity<?php echo form_error('card_id') ?></label>
            <input id="card_name" type="text" class="form-control" name="card_name" placeholder="SKU, Example : LOB-001">
            <select id="card_rarity" type="text" class="form-control" name="card_rarity" placeholder="Card Rarity" style="display:none">
                <?php foreach($rarities as $r){?>
                    <option value="<?php echo $r->name; ?>">Rarity - <?php echo $r->description; ?></option>
                <?php } ?>
            </select>
            <!--<button id="get_cart_data" cla  s="btn btn-primary" style="display:none">Get Card Data</button> -->
        </div>
        <div class="form-group" style="display:none">
            <label for="int">Card Reference ID <?php echo form_error('card_reference_id') ?></label>
            <input type="hidden" class="form-control" id="card_reference_id" name="card_reference_id" value="<?php echo $card_reference_id; ?>" />
        </div>
        <div class="form-group" style="display:none">
            <label for="int">SKU <?php echo form_error('sku') ?></label>
            <input type="text" class="form-control" id="sku" name="sku" value="<?php echo $sku; ?>" />
        </div>
        <div class="form-group" style="display:none">
            <label for="int">Rarity <?php echo form_error('rarity') ?></label>
            <input type="text" class="form-control" id="rarity" name="rarity" value="<?php echo $rarity; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Currency <?php echo form_error('currency') ?></label>
            <input type="text" class="form-control" name="currency" id="currency" placeholder="Currency" value="<?php echo $currency; ?>" />
        </div>
	    <div class="form-group">
            <label for="double">Price <?php echo form_error('price') ?></label>
            <input type="text" class="form-control" name="price" id="price" placeholder="Price" value="<?php echo $price; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Qty <?php echo form_error('qty') ?></label>
            <input type="text" class="form-control" name="qty" id="qty" placeholder="Qty" value="<?php echo $qty; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('products') ?>" class="btn btn-default">Cancel</a>
	</form><?php $this->load->view('templates/footer');?>

    <!-- JS file -->
    <script src="<?php echo base_url('assets/vendor/easyautocomplete/jquery.easy-autocomplete.min.js') ?>"></script>
    
    <script>
        var options = {
            url: function(phrase) {
                return "<?php echo site_url('products/search_autocomplete'); ?>?keyword=" + phrase + "&format=json";
            },

            getValue: "name",
            list: {
                onClickEvent: function() {
                    var sku = $("#card_name").getSelectedItemData().sku;
                    var rarity = $("#card_name").getSelectedItemData().rarity;
                    get_cart_data(sku, rarity);
                }	
            }
        };

        $("#card_name").easyAutocomplete(options);
    </script>