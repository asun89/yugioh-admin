<?php $this->load->view('templates/header');?>
        <div class="row" style="margin-bottom: 20px">
            <div class="col-md-4">
                <h2>Products List</h2>
            </div>
            <div class="col-md-4 text-center">
                <div id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-4 text-right">
				<div style="margin-top:20px;">
                <?php echo anchor(site_url('products/create'), 'Create', 'class="btn btn-primary"'); ?>
                <!-- <?php echo anchor(site_url('products/create_from_packs'), 'Create From Packs', 'class="btn btn-primary"'); ?> -->
                <a href="#" id="create_from_packs" class="btn btn-primary">Create From Packs</a>
                <script type="text/javascript">
                    // Capture click event
                    document.getElementById("create_from_packs").addEventListener("click", function() {
                        var packInput = prompt("Please enter pack name:", "");
                        if (packInput != null) {
                            document.location = "<?php echo site_url('products/create_from_packs'); ?>/" + packInput
                        }
                    });
                </script>
		<?php echo anchor(site_url('products/excel'), 'Excel', 'class="btn btn-primary"'); ?>
		<?php echo anchor(site_url('products/excel_tokopedia'), 'Excel Tokopedia', 'class="btn btn-primary"'); ?>
	    </div></div>
        </div>
        <table class="table table-bordered table-striped" id="mytable">
            <thead>
                <tr>
                    <th width="80px">No</th>
		    <th>Card Reference Id</th>
		    <th>SKU</th>
		    <th>Rarity</th>
		    <th>Name</th>
		    <th>Currency</th>
		    <th>Price</th>
		    <th>Qty</th>
		    <th width="200px">Action</th>
                </tr>
            </thead>
	    
        </table><?php $this->load->view('templates/footer'); ?><script type="text/javascript">
            $(document).ready(function() {
                $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                {
                    return {
                        "iStart": oSettings._iDisplayStart,
                        "iEnd": oSettings.fnDisplayEnd(),
                        "iLength": oSettings._iDisplayLength,
                        "iTotal": oSettings.fnRecordsTotal(),
                        "iFilteredTotal": oSettings.fnRecordsDisplay(),
                        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                    };
                };

                var t = $("#mytable").dataTable({
                    initComplete: function() {
                        var api = this.api();
                        $('#mytable_filter input')
                                .off('.DT')
                                .on('keyup.DT', function(e) {
                                    if (e.keyCode == 13) {
                                        api.search(this.value).draw();
                            }
                        });
                    },
                    oLanguage: {
                        sProcessing: "loading..."
                    },
                    processing: true,
                    serverSide: true,
                    ajax: {"url": "products/json", "type": "POST"},
                    columns: [
                        {
                            "data": "product_id",
                            "orderable": false
                        },{"data": "card_reference_id"},{"data": "sku"}, {"data": "rarity"}, {"data": "name"}, {"data": "currency"},{"data": "price"},{"data": "qty"},
                        {
                            "data" : "action",
                            "orderable": false,
                            "className" : "text-center"
                        }
                    ],
                    order: [[0, 'desc']],
                    rowCallback: function(row, data, iDisplayIndex) {
                        var info = this.fnPagingInfo();
                        var page = info.iPage;
                        var length = info.iLength;
                        var index = page * length + (iDisplayIndex + 1);
                        $('td:eq(0)', row).html(index);
                    }
                });
            });
        </script>
