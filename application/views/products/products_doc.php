<!doctype html>
<html>
    <head>
        <title>SOCIANOVATION - Web Administration</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Products List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Card Reference Id</th>
		<th>SKU</th>
		<th>Rarity</th>
		<th>Currency</th>
		<th>Price</th>
		<th>Qty</th>
		<th>Promo Id</th>
		<th>Created Datetime</th>
		<th>Updated Datetime</th>
		<th>Created By</th>
		<th>Updated By</th>
		
            </tr><?php
            foreach ($products_data as $products)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $products->card_reference_id ?></td>
		      <td><?php echo $products->sku ?></td>
		      <td><?php echo $products->rarity ?></td>
		      <td><?php echo $products->currency ?></td>
		      <td><?php echo $products->price ?></td>
		      <td><?php echo $products->qty ?></td>
		      <td><?php echo $products->promo_id ?></td>
		      <td><?php echo $products->created_datetime ?></td>
		      <td><?php echo $products->updated_datetime ?></td>
		      <td><?php echo $products->created_by ?></td>
		      <td><?php echo $products->updated_by ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>
