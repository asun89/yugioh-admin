<?php $this->load->view('templates/header');?>
<div class="row" style="margin-bottom: 20px">
            <div class="col-md-4">
                <h2>Products Create From Packs - <?php echo $selected_pack; ?></h2>
            </div>
            <div class="col-md-8 text-center">
                <div id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
        </div>

		<form action="<?php echo $action; ?>" method="post">
        <table class="table">
	    <tr>
			<td>Name</td>
			<td>SKU</td>
			<td>Rarity</td>
			<td>Qty</td>
			<td>Select</td>
		</tr>
		<?php foreach($data as $d) { ?>
		<tr>
			<td><?php echo $d->name; ?></td>
			<td><?php echo $d->sku; ?></td>
			<td><?php echo $d->rarity; ?></td>
			<td><input type="text" name="<?php echo $d->reference_id; ?>" value="3" <?php if($d->product_id != "") {echo 'disabled';}?> /></td>
			<td><input checked type="checkbox" value="<?php echo $d->reference_id; ?>" name="checked[]" <?php if($d->product_id != "") {echo 'disabled';}?> /></td>
		</tr>
		
		
		<?php } ?>
	</table>
		<input type="hidden" name="pack_code" value="<?php echo $pack_code; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
		</form>
	<?php $this->load->view('templates/footer');?>
