<!doctype html>
<html>
    <head>
        <title>SOCIANOVATION - Web Administration</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Packs List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Pack Code</th>
		<th>Pack Name</th>
		<th>Num Of Cards</th>
		<th>Release Date</th>
		<th>Pack Image</th>
		<th>Created Datetime</th>
		<th>Updated Datetime</th>
		<th>Created By</th>
		<th>Updated By</th>
		
            </tr><?php
            foreach ($packs_data as $packs)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $packs->pack_code ?></td>
		      <td><?php echo $packs->pack_name ?></td>
		      <td><?php echo $packs->num_of_cards ?></td>
		      <td><?php echo $packs->release_date ?></td>
		      <td><?php echo $packs->pack_image ?></td>
		      <td><?php echo $packs->created_datetime ?></td>
		      <td><?php echo $packs->updated_datetime ?></td>
		      <td><?php echo $packs->created_by ?></td>
		      <td><?php echo $packs->updated_by ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>