<?php $this->load->view('templates/header');?>
<div class="row" style="margin-bottom: 20px">
            <div class="col-md-4">
                <h2>Packs <?php echo $button ?></h2>
            </div>
            <div class="col-md-8 text-center">
                <div id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
        </div>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Pack Code <?php echo form_error('pack_code') ?></label>
            <input type="text" class="form-control" name="pack_code" id="pack_code" placeholder="Pack Code" value="<?php echo $pack_code; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Pack Name <?php echo form_error('pack_name') ?></label>
            <input type="text" class="form-control" name="pack_name" id="pack_name" placeholder="Pack Name" value="<?php echo $pack_name; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Num Of Cards <?php echo form_error('num_of_cards') ?></label>
            <input type="text" class="form-control" name="num_of_cards" id="num_of_cards" placeholder="Num Of Cards" value="<?php echo $num_of_cards; ?>" />
        </div>
	    <div class="form-group">
            <label for="date">Release Date <?php echo form_error('release_date') ?></label>
            <input type="text" class="form-control" name="release_date" id="release_date" placeholder="Release Date" value="<?php echo $release_date; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Pack Image <?php echo form_error('pack_image') ?></label>
            <input type="text" class="form-control" name="pack_image" id="pack_image" placeholder="Pack Image" value="<?php echo $pack_image; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Created Datetime <?php echo form_error('created_datetime') ?></label>
            <input type="text" class="form-control" name="created_datetime" id="created_datetime" placeholder="Created Datetime" value="<?php echo $created_datetime; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Updated Datetime <?php echo form_error('updated_datetime') ?></label>
            <input type="text" class="form-control" name="updated_datetime" id="updated_datetime" placeholder="Updated Datetime" value="<?php echo $updated_datetime; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Created By <?php echo form_error('created_by') ?></label>
            <input type="text" class="form-control" name="created_by" id="created_by" placeholder="Created By" value="<?php echo $created_by; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Updated By <?php echo form_error('updated_by') ?></label>
            <input type="text" class="form-control" name="updated_by" id="updated_by" placeholder="Updated By" value="<?php echo $updated_by; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('packs') ?>" class="btn btn-default">Cancel</a>
	</form><?php $this->load->view('templates/footer');?>