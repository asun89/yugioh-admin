<?php $this->load->view('templates/header');?>
<div class="row" style="margin-bottom: 20px">
            <div class="col-md-4">
                <h2>Packs Read</h2>
            </div>
            <div class="col-md-8 text-center">
                <div id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
        </div>
        <table class="table">
	    <tr><td>Pack Code</td><td><?php echo $pack_code; ?></td></tr>
	    <tr><td>Pack Name</td><td><?php echo $pack_name; ?></td></tr>
	    <tr><td>Num Of Cards</td><td><?php echo $num_of_cards; ?></td></tr>
	    <tr><td>Release Date</td><td><?php echo $release_date; ?></td></tr>
	    <tr><td>Pack Image</td><td><img width="300px" src="<?php echo $pack_image; ?>"/></td></tr>
	    <tr><td>Created Datetime</td><td><?php echo $created_datetime; ?></td></tr>
	    <tr><td>Updated Datetime</td><td><?php echo $updated_datetime; ?></td></tr>
	    <tr><td>Created By</td><td><?php echo $created_by; ?></td></tr>
	    <tr><td>Updated By</td><td><?php echo $updated_by; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('packs') ?>" class="btn btn-default">Cancel</a></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('products/create_from_packs/'. $pack_code) ?>" class="btn btn-default">Create Multiple Products from this Pack</a></td></tr>
	</table><?php $this->load->view('templates/footer');?>