<!doctype html>
<html>
    <head>
        <title>SOCIANOVATION - Web Administration</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Transaction_payments List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Transaction Id</th>
		<th>Payment Method Id</th>
		<th>Account No</th>
		<th>Account Owner</th>
		<th>Amount</th>
		<th>Status</th>
		<th>Created Datetime</th>
		<th>Updated Datetime</th>
		<th>Created By</th>
		<th>Updated By</th>
		
            </tr><?php
            foreach ($transaction_payments_data as $transaction_payments)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $transaction_payments->transaction_id ?></td>
		      <td><?php echo $transaction_payments->payment_method_id ?></td>
		      <td><?php echo $transaction_payments->account_no ?></td>
		      <td><?php echo $transaction_payments->account_owner ?></td>
		      <td><?php echo $transaction_payments->amount ?></td>
		      <td><?php echo $transaction_payments->status ?></td>
		      <td><?php echo $transaction_payments->created_datetime ?></td>
		      <td><?php echo $transaction_payments->updated_datetime ?></td>
		      <td><?php echo $transaction_payments->created_by ?></td>
		      <td><?php echo $transaction_payments->updated_by ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>