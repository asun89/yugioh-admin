<?php $this->load->view('templates/header');?>
<div class="row" style="margin-bottom: 20px">
            <div class="col-md-4">
                <h2>Transaction payments <?php echo $button ?></h2>
            </div>
            <div class="col-md-8 text-center">
                <div id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
        </div>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Transaction Id <?php echo form_error('transaction_id') ?></label>
            <input type="text" class="form-control" name="transaction_id" id="transaction_id" placeholder="Transaction Id" value="<?php echo $transaction_id; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Payment Method Id <?php echo form_error('payment_method_id') ?></label>
            <input type="text" class="form-control" name="payment_method_id" id="payment_method_id" placeholder="Payment Method Id" value="<?php echo $payment_method_id; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Account No <?php echo form_error('account_no') ?></label>
            <input type="text" class="form-control" name="account_no" id="account_no" placeholder="Account No" value="<?php echo $account_no; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Account Owner <?php echo form_error('account_owner') ?></label>
            <input type="text" class="form-control" name="account_owner" id="account_owner" placeholder="Account Owner" value="<?php echo $account_owner; ?>" />
        </div>
	    <div class="form-group">
            <label for="double">Amount <?php echo form_error('amount') ?></label>
            <input type="text" class="form-control" name="amount" id="amount" placeholder="Amount" value="<?php echo $amount; ?>" />
        </div>
	    <div class="form-group">
            <label for="tinyint">Status <?php echo form_error('status') ?></label>
            <input type="text" class="form-control" name="status" id="status" placeholder="Status" value="<?php echo $status; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Created Datetime <?php echo form_error('created_datetime') ?></label>
            <input type="text" class="form-control" name="created_datetime" id="created_datetime" placeholder="Created Datetime" value="<?php echo $created_datetime; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Updated Datetime <?php echo form_error('updated_datetime') ?></label>
            <input type="text" class="form-control" name="updated_datetime" id="updated_datetime" placeholder="Updated Datetime" value="<?php echo $updated_datetime; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Created By <?php echo form_error('created_by') ?></label>
            <input type="text" class="form-control" name="created_by" id="created_by" placeholder="Created By" value="<?php echo $created_by; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Updated By <?php echo form_error('updated_by') ?></label>
            <input type="text" class="form-control" name="updated_by" id="updated_by" placeholder="Updated By" value="<?php echo $updated_by; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('transaction_payments') ?>" class="btn btn-default">Cancel</a>
	</form><?php $this->load->view('templates/footer');?>