<!doctype html>
<html>
    <head>
        <title>SOCIANOVATION - Web Administration</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Categories List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Product Id</th>
		<th>Name</th>
		<th>Value</th>
		<th>Caption</th>
		
            </tr><?php
            foreach ($categories_data as $categories)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $categories->product_id ?></td>
		      <td><?php echo $categories->name ?></td>
		      <td><?php echo $categories->value ?></td>
		      <td><?php echo $categories->caption ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>