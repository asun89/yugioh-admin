<!doctype html>
<html>
    <head>
        <title>SOCIANOVATION - Web Administration</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Accounts List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Username</th>
		<th>Password</th>
		<th>Fullname</th>
		<th>Email</th>
		<th>Phone</th>
		<th>Created Datetime</th>
		<th>Updated Datetime</th>
		<th>Created By</th>
		<th>Updated By</th>
		
            </tr><?php
            foreach ($accounts_data as $accounts)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $accounts->username ?></td>
		      <td><?php echo $accounts->password ?></td>
		      <td><?php echo $accounts->fullname ?></td>
		      <td><?php echo $accounts->email ?></td>
		      <td><?php echo $accounts->phone ?></td>
		      <td><?php echo $accounts->created_datetime ?></td>
		      <td><?php echo $accounts->updated_datetime ?></td>
		      <td><?php echo $accounts->created_by ?></td>
		      <td><?php echo $accounts->updated_by ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>