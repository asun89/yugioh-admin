<?php $this->load->view('templates/header');?>
<div class="row" style="margin-bottom: 20px">
            <div class="col-md-4">
                <h2>Cards <?php echo $button ?></h2>
            </div>
            <div class="col-md-8 text-center">
                <div id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
        </div>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Name <?php echo form_error('name') ?></label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Sku <?php echo form_error('sku') ?></label>
            <input type="text" class="form-control" name="sku" id="sku" placeholder="Sku" value="<?php echo $sku; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Rarity <?php echo form_error('rarity') ?></label>
            <input type="text" class="form-control" name="rarity" id="rarity" placeholder="Rarity" value="<?php echo $rarity; ?>" />
        </div>
	    <div class="form-group">
            <label for="bigint">Reference Id <?php echo form_error('reference_id') ?></label>
            <input type="text" class="form-control" name="reference_id" id="reference_id" placeholder="Reference Id" value="<?php echo $reference_id; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Original Currency <?php echo form_error('original_currency') ?></label>
            <input type="text" class="form-control" name="original_currency" id="original_currency" placeholder="Original Currency" value="<?php echo $original_currency; ?>" />
        </div>
	    <div class="form-group">
            <label for="float">Original Price <?php echo form_error('original_price') ?></label>
            <input type="text" class="form-control" name="original_price" id="original_price" placeholder="Original Price" value="<?php echo $original_price; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Currency <?php echo form_error('currency') ?></label>
            <input type="text" class="form-control" name="currency" id="currency" placeholder="Currency" value="<?php echo $currency; ?>" />
        </div>
	    <div class="form-group">
            <label for="float">Price <?php echo form_error('price') ?></label>
            <input type="text" class="form-control" name="price" id="price" placeholder="Price" value="<?php echo $price; ?>" />
        </div>
	    <div class="form-group">
            <label for="metadata_json">Metadata Json <?php echo form_error('metadata_json') ?></label>
            <textarea class="form-control" rows="3" name="metadata_json" id="metadata_json" placeholder="Metadata Json"><?php echo $metadata_json; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="datetime">Created Datetime <?php echo form_error('created_datetime') ?></label>
            <input type="text" class="form-control" name="created_datetime" id="created_datetime" placeholder="Created Datetime" value="<?php echo $created_datetime; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Updated Datetime <?php echo form_error('updated_datetime') ?></label>
            <input type="text" class="form-control" name="updated_datetime" id="updated_datetime" placeholder="Updated Datetime" value="<?php echo $updated_datetime; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Created By <?php echo form_error('created_by') ?></label>
            <input type="text" class="form-control" name="created_by" id="created_by" placeholder="Created By" value="<?php echo $created_by; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Updated By <?php echo form_error('updated_by') ?></label>
            <input type="text" class="form-control" name="updated_by" id="updated_by" placeholder="Updated By" value="<?php echo $updated_by; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('cards') ?>" class="btn btn-default">Cancel</a>
	</form><?php $this->load->view('templates/footer');?>