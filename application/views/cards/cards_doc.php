<!doctype html>
<html>
    <head>
        <title>SOCIANOVATION - Web Administration</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Cards List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Name</th>
		<th>Sku</th>
		<th>Rarity</th>
		<th>Reference Id</th>
		<th>Original Currency</th>
		<th>Original Price</th>
		<th>Currency</th>
		<th>Price</th>
		<th>Metadata Json</th>
		<th>Created Datetime</th>
		<th>Updated Datetime</th>
		<th>Created By</th>
		<th>Updated By</th>
		
            </tr><?php
            foreach ($cards_data as $cards)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $cards->name ?></td>
		      <td><?php echo $cards->sku ?></td>
		      <td><?php echo $cards->rarity ?></td>
		      <td><?php echo $cards->reference_id ?></td>
		      <td><?php echo $cards->original_currency ?></td>
		      <td><?php echo $cards->original_price ?></td>
		      <td><?php echo $cards->currency ?></td>
		      <td><?php echo $cards->price ?></td>
		      <td><?php echo $cards->metadata_json ?></td>
		      <td><?php echo $cards->created_datetime ?></td>
		      <td><?php echo $cards->updated_datetime ?></td>
		      <td><?php echo $cards->created_by ?></td>
		      <td><?php echo $cards->updated_by ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>