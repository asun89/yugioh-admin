<!doctype html>
<html>
    <head>
        <title>SOCIANOVATION - Web Administration</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Provinces List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Name</th>
		<th>Created Datetime</th>
		<th>Updated Datetime</th>
		<th>Created By</th>
		<th>Updated By</th>
		
            </tr><?php
            foreach ($provinces_data as $provinces)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $provinces->name ?></td>
		      <td><?php echo $provinces->created_datetime ?></td>
		      <td><?php echo $provinces->updated_datetime ?></td>
		      <td><?php echo $provinces->created_by ?></td>
		      <td><?php echo $provinces->updated_by ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>