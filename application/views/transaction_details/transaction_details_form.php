<?php $this->load->view('templates/header');?>
<div class="row" style="margin-bottom: 20px">
            <div class="col-md-4">
                <h2>Transaction details <?php echo $button ?></h2>
            </div>
            <div class="col-md-8 text-center">
                <div id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
        </div>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Transaction Id <?php echo form_error('transaction_id') ?></label>
            <input type="text" class="form-control" name="transaction_id" id="transaction_id" placeholder="Transaction Id" value="<?php echo $transaction_id; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Product Id <?php echo form_error('product_id') ?></label>
            <input type="text" class="form-control" name="product_id" id="product_id" placeholder="Product Id" value="<?php echo $product_id; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Qty <?php echo form_error('qty') ?></label>
            <input type="text" class="form-control" name="qty" id="qty" placeholder="Qty" value="<?php echo $qty; ?>" />
        </div>
	    <div class="form-group">
            <label for="double">Price <?php echo form_error('price') ?></label>
            <input type="text" class="form-control" name="price" id="price" placeholder="Price" value="<?php echo $price; ?>" />
        </div>
	    <div class="form-group">
            <label for="double">Subtotal <?php echo form_error('subtotal') ?></label>
            <input type="text" class="form-control" name="subtotal" id="subtotal" placeholder="Subtotal" value="<?php echo $subtotal; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('transaction_details') ?>" class="btn btn-default">Cancel</a>
	</form><?php $this->load->view('templates/footer');?>