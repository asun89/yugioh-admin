<!doctype html>
<html>
    <head>
        <title>SOCIANOVATION - Web Administration</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Transaction_details List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Transaction Id</th>
		<th>Product Id</th>
		<th>Qty</th>
		<th>Price</th>
		<th>Subtotal</th>
		
            </tr><?php
            foreach ($transaction_details_data as $transaction_details)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $transaction_details->transaction_id ?></td>
		      <td><?php echo $transaction_details->product_id ?></td>
		      <td><?php echo $transaction_details->qty ?></td>
		      <td><?php echo $transaction_details->price ?></td>
		      <td><?php echo $transaction_details->subtotal ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>