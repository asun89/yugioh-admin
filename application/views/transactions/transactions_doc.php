<!doctype html>
<html>
    <head>
        <title>SOCIANOVATION - Web Administration</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Transactions List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Total Price</th>
		<th>Total Discount</th>
		<th>Total Markup</th>
		<th>Account Id</th>
		<th>Customer Title</th>
		<th>Customer Fullname</th>
		<th>Customer Email</th>
		<th>Customer Phone</th>
		<th>Customer Address</th>
		<th>Customer Tracker Key</th>
		<th>Status</th>
		<th>Created Datetime</th>
		<th>Updated Datetime</th>
		<th>Created By</th>
		<th>Updated By</th>
		
            </tr><?php
            foreach ($transactions_data as $transactions)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $transactions->total_price ?></td>
		      <td><?php echo $transactions->total_discount ?></td>
		      <td><?php echo $transactions->total_markup ?></td>
		      <td><?php echo $transactions->account_id ?></td>
		      <td><?php echo $transactions->customer_title ?></td>
		      <td><?php echo $transactions->customer_fullname ?></td>
		      <td><?php echo $transactions->customer_email ?></td>
		      <td><?php echo $transactions->customer_phone ?></td>
		      <td><?php echo $transactions->customer_address ?></td>
		      <td><?php echo $transactions->customer_tracker_key ?></td>
		      <td><?php echo $transactions->status ?></td>
		      <td><?php echo $transactions->created_datetime ?></td>
		      <td><?php echo $transactions->updated_datetime ?></td>
		      <td><?php echo $transactions->created_by ?></td>
		      <td><?php echo $transactions->updated_by ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>