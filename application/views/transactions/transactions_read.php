<?php $this->load->view('templates/header');?>
<div class="row" style="margin-bottom: 20px">
            <div class="col-md-4">
                <h2>Transactions Read</h2>
            </div>
            <div class="col-md-8 text-center">
                <div id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
        </div>
        <table class="table">
	    <tr><td>Total Price</td><td><?php echo $total_price; ?></td></tr>
	    <tr><td>Total Discount</td><td><?php echo $total_discount; ?></td></tr>
	    <tr><td>Total Markup</td><td><?php echo $total_markup; ?></td></tr>
	    <tr><td>Account Id</td><td><?php echo $account_id; ?></td></tr>
	    <tr><td>Customer Title</td><td><?php echo $customer_title; ?></td></tr>
	    <tr><td>Customer Fullname</td><td><?php echo $customer_fullname; ?></td></tr>
	    <tr><td>Customer Email</td><td><?php echo $customer_email; ?></td></tr>
	    <tr><td>Customer Phone</td><td><?php echo $customer_phone; ?></td></tr>
	    <tr><td>Customer Address</td><td><?php echo $customer_address; ?></td></tr>
	    <tr><td>Customer Tracker Key</td><td><?php echo $customer_tracker_key; ?></td></tr>
	    <tr><td>Status</td><td><?php echo $status; ?></td></tr>
	    <tr><td>Created Datetime</td><td><?php echo $created_datetime; ?></td></tr>
	    <tr><td>Updated Datetime</td><td><?php echo $updated_datetime; ?></td></tr>
	    <tr><td>Created By</td><td><?php echo $created_by; ?></td></tr>
	    <tr><td>Updated By</td><td><?php echo $updated_by; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('transactions') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table><?php $this->load->view('templates/footer');?>