<?php $this->load->view('templates/header');?>
<div class="row" style="margin-bottom: 20px">
            <div class="col-md-4">
                <h2>Transactions <?php echo $button ?></h2>
            </div>
            <div class="col-md-8 text-center">
                <div id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
        </div>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Total Price <?php echo form_error('total_price') ?></label>
            <input type="text" class="form-control" name="total_price" id="total_price" placeholder="Total Price" value="<?php echo $total_price; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Total Discount <?php echo form_error('total_discount') ?></label>
            <input type="text" class="form-control" name="total_discount" id="total_discount" placeholder="Total Discount" value="<?php echo $total_discount; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Total Markup <?php echo form_error('total_markup') ?></label>
            <input type="text" class="form-control" name="total_markup" id="total_markup" placeholder="Total Markup" value="<?php echo $total_markup; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Account Id <?php echo form_error('account_id') ?></label>
            <input type="text" class="form-control" name="account_id" id="account_id" placeholder="Account Id" value="<?php echo $account_id; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Customer Title <?php echo form_error('customer_title') ?></label>
            <input type="text" class="form-control" name="customer_title" id="customer_title" placeholder="Customer Title" value="<?php echo $customer_title; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Customer Fullname <?php echo form_error('customer_fullname') ?></label>
            <input type="text" class="form-control" name="customer_fullname" id="customer_fullname" placeholder="Customer Fullname" value="<?php echo $customer_fullname; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Customer Email <?php echo form_error('customer_email') ?></label>
            <input type="text" class="form-control" name="customer_email" id="customer_email" placeholder="Customer Email" value="<?php echo $customer_email; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Customer Phone <?php echo form_error('customer_phone') ?></label>
            <input type="text" class="form-control" name="customer_phone" id="customer_phone" placeholder="Customer Phone" value="<?php echo $customer_phone; ?>" />
        </div>
	    <div class="form-group">
            <label for="customer_address">Customer Address <?php echo form_error('customer_address') ?></label>
            <textarea class="form-control" rows="3" name="customer_address" id="customer_address" placeholder="Customer Address"><?php echo $customer_address; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="varchar">Customer Tracker Key <?php echo form_error('customer_tracker_key') ?></label>
            <input type="text" class="form-control" name="customer_tracker_key" id="customer_tracker_key" placeholder="Customer Tracker Key" value="<?php echo $customer_tracker_key; ?>" />
        </div>
	    <div class="form-group">
            <label for="tinyint">Status <?php echo form_error('status') ?></label>
            <input type="text" class="form-control" name="status" id="status" placeholder="Status" value="<?php echo $status; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Created Datetime <?php echo form_error('created_datetime') ?></label>
            <input type="text" class="form-control" name="created_datetime" id="created_datetime" placeholder="Created Datetime" value="<?php echo $created_datetime; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Updated Datetime <?php echo form_error('updated_datetime') ?></label>
            <input type="text" class="form-control" name="updated_datetime" id="updated_datetime" placeholder="Updated Datetime" value="<?php echo $updated_datetime; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Created By <?php echo form_error('created_by') ?></label>
            <input type="text" class="form-control" name="created_by" id="created_by" placeholder="Created By" value="<?php echo $created_by; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Updated By <?php echo form_error('updated_by') ?></label>
            <input type="text" class="form-control" name="updated_by" id="updated_by" placeholder="Updated By" value="<?php echo $updated_by; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('transactions') ?>" class="btn btn-default">Cancel</a>
	</form><?php $this->load->view('templates/footer');?>