<!doctype html>
<html>
    <head>
        <title>SOCIANOVATION - Web Administration</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Currency List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Currency From</th>
		<th>Currency To</th>
		<th>Buy Price</th>
		<th>Sell Price</th>
		<th>Created Datetime</th>
		<th>Created By</th>
		<th>Updated Datetime</th>
		<th>Updated By</th>
		
            </tr><?php
            foreach ($currency_data as $currency)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $currency->currency_from ?></td>
		      <td><?php echo $currency->currency_to ?></td>
		      <td><?php echo $currency->buy_price ?></td>
		      <td><?php echo $currency->sell_price ?></td>
		      <td><?php echo $currency->created_datetime ?></td>
		      <td><?php echo $currency->created_by ?></td>
		      <td><?php echo $currency->updated_datetime ?></td>
		      <td><?php echo $currency->updated_by ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>