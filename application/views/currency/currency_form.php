<?php $this->load->view('templates/header');?>
<div class="row" style="margin-bottom: 20px">
            <div class="col-md-4">
                <h2>Currency <?php echo $button ?></h2>
            </div>
            <div class="col-md-8 text-center">
                <div id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
        </div>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Currency From <?php echo form_error('currency_from') ?></label>
            <input type="text" class="form-control" name="currency_from" id="currency_from" placeholder="Currency From" value="<?php echo $currency_from; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Currency To <?php echo form_error('currency_to') ?></label>
            <input type="text" class="form-control" name="currency_to" id="currency_to" placeholder="Currency To" value="<?php echo $currency_to; ?>" />
        </div>
	    <div class="form-group">
            <label for="float">Buy Price <?php echo form_error('buy_price') ?></label>
            <input type="text" class="form-control" name="buy_price" id="buy_price" placeholder="Buy Price" value="<?php echo $buy_price; ?>" />
        </div>
	    <div class="form-group">
            <label for="float">Sell Price <?php echo form_error('sell_price') ?></label>
            <input type="text" class="form-control" name="sell_price" id="sell_price" placeholder="Sell Price" value="<?php echo $sell_price; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Created Datetime <?php echo form_error('created_datetime') ?></label>
            <input type="text" class="form-control" name="created_datetime" id="created_datetime" placeholder="Created Datetime" value="<?php echo $created_datetime; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Created By <?php echo form_error('created_by') ?></label>
            <input type="text" class="form-control" name="created_by" id="created_by" placeholder="Created By" value="<?php echo $created_by; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Updated Datetime <?php echo form_error('updated_datetime') ?></label>
            <input type="text" class="form-control" name="updated_datetime" id="updated_datetime" placeholder="Updated Datetime" value="<?php echo $updated_datetime; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Updated By <?php echo form_error('updated_by') ?></label>
            <input type="text" class="form-control" name="updated_by" id="updated_by" placeholder="Updated By" value="<?php echo $updated_by; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('currency') ?>" class="btn btn-default">Cancel</a>
	</form><?php $this->load->view('templates/footer');?>