
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Generated_excel_model extends CI_Model
{

    public $table = 'generated_excels ge';
    public $table_insert = 'generated_excels';
    public $id = 'ge.id';
    public $update_id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table_insert, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->update_id, $id);
        $this->db->update($this->table_insert, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->update_id, $id);
        $this->db->delete($this->table_insert);
    }

	// find top row
    function find_top()
    {
        $sql = "SELECT * FROM ".$this->table. " ORDER BY ".$this->id." DESC LIMIT 1";
		return $this->db->query($sql)->row_array();
    }

}
