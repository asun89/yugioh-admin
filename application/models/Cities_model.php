<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cities_model extends CI_Model
{

    public $table = 'cities';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function json() {
        $this->datatables->select('id,province_id,province,type,name,postal_code,created_datetime,updated_datetime,created_by,updated_by');
        $this->datatables->from('cities');
        //add this line for join
        //$this->datatables->join('table2', 'cities.field = table2.field');
        $this->datatables->add_column('action', anchor(site_url('cities/read/$1'),'Read')." | ".anchor(site_url('cities/update/$1'),'Update')." | ".anchor(site_url('cities/delete/$1'),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'id');
        return $this->datatables->generate();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id', $q);
	$this->db->or_like('province_id', $q);
	$this->db->or_like('province', $q);
	$this->db->or_like('type', $q);
	$this->db->or_like('name', $q);
	$this->db->or_like('postal_code', $q);
	$this->db->or_like('created_datetime', $q);
	$this->db->or_like('updated_datetime', $q);
	$this->db->or_like('created_by', $q);
	$this->db->or_like('updated_by', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id', $q);
	$this->db->or_like('province_id', $q);
	$this->db->or_like('province', $q);
	$this->db->or_like('type', $q);
	$this->db->or_like('name', $q);
	$this->db->or_like('postal_code', $q);
	$this->db->or_like('created_datetime', $q);
	$this->db->or_like('updated_datetime', $q);
	$this->db->or_like('created_by', $q);
	$this->db->or_like('updated_by', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

    function delete_all()
    {
        $this->db->empty_table($this->table);
    }

}

/* End of file Cities_model.php */
/* Location: ./application/models/Cities_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-11-22 14:34:45 */
/* http://harviacode.com */