<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Scrapping_model extends CI_model {

    public function __construct()
    {
            parent::__construct();
            $this->load->helper("simple_html_dom");
    }
    
	public function get_description_by_card_name($name){
        $html = file_get_html("https://yugipedia.com/wiki/".str_replace(" ", "_", $name));
        foreach($html->find('.lore') as $element){
            return trim(preg_replace("/[\\n\\r]+/", "", $element->plaintext));
        }

        return "";
    }

    private function tdrows($elements)
    {
        $str = "";
        foreach ($elements as $element) {
            $str .= $element->nodeValue . ", ";
        }

        return $str;
    }

    public function get_card_rarity_by_name_and_sku($name, $sku){
        $yugioh_page = file_get_contents("https://yugipedia.com/wiki/".str_replace(" ", "_", $name));
        preg_match_all('/<table id="cts--EN" class="wikitable sortable card-list cts">(.*)<\/table>/', $yugioh_page, $output_array);

        $DOM = new DOMDocument;
        $DOM->loadHTML($output_array[0][0]);

        $items = $DOM->getElementsByTagName('tr');

        foreach ($items as $node) {
            $split = explode(",", $this->tdrows($node->childNodes));
            if(strtoupper(trim($split[1])) == strtoupper(trim($sku))){
                return $split[3];
            }
        }
    }

    public function get_card_price_by_sku_from_tnt($sku_rarity){
        $yugioh_page = file_get_contents("https://www.trollandtoad.com");
        preg_match_all('/name=\'token\' value=\'(.*)\'>/', $yugioh_page, $output_array);
        $html = file_get_html("https://www.trollandtoad.com/category.php?token=".$output_array[1][0]."&search-words=".$sku_rarity."&selected-cat=0");
        $output = "";
        foreach($html->find('.buying-options-table') as $element){
            $output = trim(preg_replace("/[\\n\\r]+/", "", $element->plaintext));
            break;
        }
        $data = explode("$", $output);
        $price_data = explode(" ", $data[1]);
        
        return $price_data[0];
    }

    public function get_all_ygoprodeck_data($force){
        $response = file_get_contents("https://db.ygoprodeck.com/api/v7/checkDBVer.php");
        if(!$response) {
            return false;
        }
        $version = json_decode($response, true)[0]['database_version'];
        
        $current_version = 0;
        $version_data = $this->db->query("SELECT version FROM ygo_db_version LIMIT 1")->row_array();
        if(!empty($version_data['version'])) {
            $current_version = $version_data['version'];
        }

        if(!$force && $current_version == $version) {
            return false;
        }

        $response = file_get_contents("https://db.ygoprodeck.com/api/v7/cardinfo.php");
        $this->scrapping_model->update_db_version($version);
        return json_decode($response, true);
    }

    public function get_all_ygoprodeck_packs_data($force){
        $response = file_get_contents("https://db.ygoprodeck.com/api/v7/checkDBVer.php");
        if(!$response) {
            return false;
        }
        $version = json_decode($response, true)[0]['database_version'];
        
        $current_version = 0;
        $version_data = $this->db->query("SELECT version FROM ygo_db_version LIMIT 1")->row_array();
        if(!empty($version_data['version'])) {
            $current_version = $version_data['version'];
        }

        if(!$force && $current_version == $version) {
            return false;
        }

        $response = file_get_contents("https://db.ygoprodeck.com/api/v7/cardsets.php");

        $this->scrapping_model->update_db_version($version);

        return json_decode($response, true);
    }

    public function update_db_version($version){
        $sql = "
		DELETE FROM ygo_db_version LIMIT 1;
        ";
        $this->db->query($sql);

        $sql = "INSERT INTO ygo_db_version VALUES('', ?, NOW())";
		$this->db->query($sql, array($version));
    }

    public function insert_new_cards($card){
        $sql = "
		INSERT INTO 
		cards
			(name, reference_id, pack_code, external_reference_id, metadata_json, sku, rarity, currency, original_currency)
		VALUES
			(?,?,?,?, ?, ?,?, ?,?)
        ";
		$this->db->query($sql, array($card['name'], $card['reference_id'], $card['pack_code'],$card['external_reference_id'], $card['metadata_json'],
	$card['sku'], $card['rarity'], $card['currency'], $card['original_currency']));
	}

    public function insert_new_packs($card){
        $sql = "
		INSERT INTO 
		packs
			(pack_code, pack_name, num_of_cards, release_date, pack_image)
		VALUES
			(?,?,?, ?,?)
        ";
		$this->db->query($sql, array($card['pack_code'], $card['pack_name'], $card['num_of_cards'], $card['release_date'],
	$card['pack_image']));
	}
	
	
	public function delete_cards(){
        $this->db->query("SET FOREIGN_KEY_CHECKS=0");
        $sql = "
		DELETE FROM
		cards";
        $this->db->query($sql);

        $this->db->query("SET FOREIGN_KEY_CHECKS=1");
	}

    public function delete_packs(){
        $this->db->query("SET FOREIGN_KEY_CHECKS=0");
        $sql = "
		DELETE FROM
		packs";
        $this->db->query($sql);

        $this->db->query("SET FOREIGN_KEY_CHECKS=1");
	}

	public function update_card_price($sku, $original_price, $price){
        $sql = "
		UPDATE cards 
			SET 
			price = ?,
			currency = ?,
			original_price = ?,
			original_currency = ?
		WHERE sku = ?";
        $this->db->query($sql, array($price, "IDR", $original_price, "USD", $sku));
	}

    public function update_product_price($card_reference_id, $price){
        $sql = "
		UPDATE products 
			SET 
			price = ?
		WHERE card_reference_id = ?";
        $this->db->query($sql, array($price, $card_reference_id));
	}
	
	public function get_cards_without_price(){
        $sql = "
		SELECT sku, rarity, card_reference_id FROM products p 
		JOIN cards c ON c.reference_id = p.card_reference_id
        WHERE p.price = 0 OR p.price IS NULL
        ORDER BY p.id ASC";
        return $this->db->query($sql)->result_array();
    }
    
    public function get_currency_rate(){
        $sql = "
        SELECT 
	        sell_price, buy_price
        FROM currency p 
		WHERE currency_from = ? AND currency_to = ?";
        return $this->db->query($sql, array("USD", "IDR"))->result_array()[0]['sell_price'];
    }
}
