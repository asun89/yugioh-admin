<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Rarities_model extends CI_Model
{

    public $table = 'rarities';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        return $this->db->get($this->table)->result();
    }
}