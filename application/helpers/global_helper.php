<?php  if ( ! defined("BASEPATH")) exit("No direct script access allowed");
	function generate_sidemenu()
	{
		return '<li>
		<a href="'.site_url('accounts').'"><i class="fa fa-list fa-fw"></i> Accounts</a>
	</li><li>
		<a href="'.site_url('admins').'"><i class="fa fa-list fa-fw"></i> Admins</a>
	</li><li>
		<a href="'.site_url('cards').'"><i class="fa fa-list fa-fw"></i> Cards</a>
	</li><li>
		<a href="'.site_url('categories').'"><i class="fa fa-list fa-fw"></i> Categories</a>
	</li><li>
		<a href="'.site_url('currency').'"><i class="fa fa-list fa-fw"></i> Currency</a>
	</li><li>
		<a href="'.site_url('products').'"><i class="fa fa-list fa-fw"></i> Products</a>
	</li><li>
		<a href="'.site_url('transaction_details').'"><i class="fa fa-list fa-fw"></i> Transaction details</a>
	</li><li>
		<a href="'.site_url('transactions').'"><i class="fa fa-list fa-fw"></i> Transactions</a>
	</li><li>
		<a href="'.site_url('transaction_payments').'"><i class="fa fa-list fa-fw"></i> Transaction Payments</a>
	</li><li>
		<a href="'.site_url('cities').'"><i class="fa fa-list fa-fw"></i> Cities</a>
	</li><li>
		<a href="'.site_url('provinces').'"><i class="fa fa-list fa-fw"></i> Provinces</a>
	</li><li>
		<a href="'.site_url('packs').'"><i class="fa fa-list fa-fw"></i> Packs</a>
	</li>';
	}
