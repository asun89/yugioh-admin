<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cities extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('Cities_model');
        $this->load->library('form_validation');

        if(!$this->session->userdata('logined') || $this->session->userdata('logined') != true)
        {
            redirect('/');
        }        
	$this->load->library('datatables');
    }

    public function index()
    {
        $this->load->view('cities/cities_list');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Cities_model->json();
    }

    public function generate(){
        $output = file_get_contents("https://api.rajaongkir.com/starter/city?key=f951369fa903d1688c9f8cc56dc47535");
        $data = json_decode($output);

        if(count($data->rajaongkir->results)){
            $this->Cities_model->delete_all();
            foreach($data->rajaongkir->results as $d){
                $d->name = $d->city_name;
                $d->created_by = 1;
                $d->updated_by = 1;
                $d->created_datetime = date("Y-m-d H:i:s");
                $d->updated_datetime = date("Y-m-d H:i:s");
                unset($d->city_name);
                unset($d->city_id);
                $this->Cities_model->insert($d);
            }
        }
        redirect("provinces");
    }

    public function read($id) 
    {
        $row = $this->Cities_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'province_id' => $row->province_id,
		'province' => $row->province,
		'type' => $row->type,
		'name' => $row->name,
		'postal_code' => $row->postal_code,
		'created_datetime' => $row->created_datetime,
		'updated_datetime' => $row->updated_datetime,
		'created_by' => $row->created_by,
		'updated_by' => $row->updated_by,
	    );
            $this->load->view('cities/cities_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('cities'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('cities/create_action'),
	    'id' => set_value('id'),
	    'province_id' => set_value('province_id'),
	    'province' => set_value('province'),
	    'type' => set_value('type'),
	    'name' => set_value('name'),
	    'postal_code' => set_value('postal_code'),
	    'created_datetime' => set_value('created_datetime'),
	    'updated_datetime' => set_value('updated_datetime'),
	    'created_by' => set_value('created_by'),
	    'updated_by' => set_value('updated_by'),
	);
        $this->load->view('cities/cities_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'province_id' => $this->input->post('province_id',TRUE),
		'province' => $this->input->post('province',TRUE),
		'type' => $this->input->post('type',TRUE),
		'name' => $this->input->post('name',TRUE),
		'postal_code' => $this->input->post('postal_code',TRUE),
		'created_datetime' => $this->input->post('created_datetime',TRUE),
		'updated_datetime' => $this->input->post('updated_datetime',TRUE),
		'created_by' => $this->input->post('created_by',TRUE),
		'updated_by' => $this->input->post('updated_by',TRUE),
	    );

            $this->Cities_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('cities'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Cities_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('cities/update_action'),
		'id' => set_value('id', $row->id),
		'province_id' => set_value('province_id', $row->province_id),
		'province' => set_value('province', $row->province),
		'type' => set_value('type', $row->type),
		'name' => set_value('name', $row->name),
		'postal_code' => set_value('postal_code', $row->postal_code),
		'created_datetime' => set_value('created_datetime', $row->created_datetime),
		'updated_datetime' => set_value('updated_datetime', $row->updated_datetime),
		'created_by' => set_value('created_by', $row->created_by),
		'updated_by' => set_value('updated_by', $row->updated_by),
	    );
            $this->load->view('cities/cities_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('cities'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'province_id' => $this->input->post('province_id',TRUE),
		'province' => $this->input->post('province',TRUE),
		'type' => $this->input->post('type',TRUE),
		'name' => $this->input->post('name',TRUE),
		'postal_code' => $this->input->post('postal_code',TRUE),
		'created_datetime' => $this->input->post('created_datetime',TRUE),
		'updated_datetime' => $this->input->post('updated_datetime',TRUE),
		'created_by' => $this->input->post('created_by',TRUE),
		'updated_by' => $this->input->post('updated_by',TRUE),
	    );

            $this->Cities_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('cities'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Cities_model->get_by_id($id);

        if ($row) {
            $this->Cities_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('cities'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('cities'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('province_id', 'province id', 'trim|required');
	$this->form_validation->set_rules('province', 'province', 'trim|required');
	$this->form_validation->set_rules('type', 'type', 'trim|required');
	$this->form_validation->set_rules('name', 'name', 'trim|required');
	$this->form_validation->set_rules('postal_code', 'postal code', 'trim|required');
	$this->form_validation->set_rules('created_datetime', 'created datetime', 'trim|required');
	$this->form_validation->set_rules('updated_datetime', 'updated datetime', 'trim|required');
	$this->form_validation->set_rules('created_by', 'created by', 'trim|required');
	$this->form_validation->set_rules('updated_by', 'updated by', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "cities.xls";
        $judul = "cities";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Province Id");
	xlsWriteLabel($tablehead, $kolomhead++, "Province");
	xlsWriteLabel($tablehead, $kolomhead++, "Type");
	xlsWriteLabel($tablehead, $kolomhead++, "Name");
	xlsWriteLabel($tablehead, $kolomhead++, "Postal Code");
	xlsWriteLabel($tablehead, $kolomhead++, "Created Datetime");
	xlsWriteLabel($tablehead, $kolomhead++, "Updated Datetime");
	xlsWriteLabel($tablehead, $kolomhead++, "Created By");
	xlsWriteLabel($tablehead, $kolomhead++, "Updated By");

	foreach ($this->Cities_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->province_id);
	    xlsWriteLabel($tablebody, $kolombody++, $data->province);
	    xlsWriteLabel($tablebody, $kolombody++, $data->type);
	    xlsWriteLabel($tablebody, $kolombody++, $data->name);
	    xlsWriteLabel($tablebody, $kolombody++, $data->postal_code);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created_datetime);
	    xlsWriteLabel($tablebody, $kolombody++, $data->updated_datetime);
	    xlsWriteNumber($tablebody, $kolombody++, $data->created_by);
	    xlsWriteNumber($tablebody, $kolombody++, $data->updated_by);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=cities.doc");

        $data = array(
            'cities_data' => $this->Cities_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('cities/cities_doc',$data);
    }

}

/* End of file Cities.php */
/* Location: ./application/controllers/Cities.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-11-22 14:34:45 */
/* http://harviacode.com */