<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Products extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('Products_model');
        $this->load->model('Generated_excel_model');
        $this->load->library('form_validation');

        if(!$this->session->userdata('logined') || $this->session->userdata('logined') != true)
        {
            redirect('/');
        }        
	$this->load->library('datatables');
    }

    public function index()
    {
        $this->load->view('products/products_list');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Products_model->json();
    }

    public function read($id) 
    {
        $row = $this->Products_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'card_reference_id' => $row->card_reference_id,
		'sku' => $row->sku,
		'rarity' => $row->rarity,
		'currency' => $row->currency,
		'price' => $row->price,
		'qty' => $row->qty,
		'promo_id' => $row->promo_id,
		'created_datetime' => $row->created_datetime,
		'updated_datetime' => $row->updated_datetime,
		'created_by' => $row->created_by,
        'updated_by' => $row->updated_by,
        'name' => $row->name,
        'sku' => $row->sku,
        'rarity' => $row->rarity
	    );
            $this->load->view('products/products_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('products'));
        }
    }

    public function create_from_packs($pack_code) 
    {
        $rarity = $this->input->get('rarity', TRUE);
        $this->load->model("Cards_model");
        $data = $this->Cards_model->get_cards_by_pack_code_and_rarity($pack_code, $rarity);
        $this->load->view('products/products_create_from_packs', 
            array("data" => $data, 
            "selected_pack" => $pack_code, 
            'action' => site_url('products/create_from_packs_action'),
            'button' => 'Submit',
            'pack_code' => $pack_code
            )
        );
    }

    public function create_from_packs_action() 
    {
        $post = $this->input->post();
        $checkeds = $post['checked'];
        $pack_code = $post['pack_code'];

        // echo '<pre>';
        // print_r($this->input->post());
        // print_r($checkeds);
        // echo '</pre>';
        // exit;

        if (!isset($checkeds) || !isset($pack_code) ) {
            $this->create_from_packs($pack_code);
        } else {
            if($this->Products_model->is_duplicate_check($this->input->post('card_reference_id',TRUE))) {
                $this->session->set_flashdata('message', 'Product with same card reference id already exist');
                redirect(site_url('products/create_from_packs/' . $pack_code));
            }

            for($a = 0; $a < count($checkeds) ; $a++) {
                if(!isset($checkeds[$a])) {
                    continue;
                }

                if($this->Products_model->is_duplicate_check($checkeds[$a])) {
                    continue;
                }
                
                $data = array(
                    'card_reference_id' => $checkeds[$a],
                    'currency' => "IDR",
                    'price' => 0,
                    'qty' => $post[$checkeds[$a]],
                    'promo_id' => NULL,
                    'created_datetime' => date("Y-m-d H:i:s"),
                    'updated_datetime' => date("Y-m-d H:i:s"),
                    'created_by' => 1,
                    'updated_by' => 1,
                );
                $this->Products_model->insert($data);
            }
            $this->session->set_flashdata('message', 'Create Multiple Products Record Success');
            redirect(site_url('products/create_from_packs/' . $pack_code));
        }
    }

    public function create() 
    {
        $this->load->model('Rarities_model');
        $data = array(
            'button' => 'Create',
            'action' => site_url('products/create_action'),
	    'id' => set_value('id'),
	    'card_reference_id' => set_value('card_reference_id'),
	    'rarity' => set_value('rarity'),
	    'sku' => set_value('sku'),
	    'currency' => set_value('currency'),
	    'price' => set_value('price'),
	    'qty' => set_value('qty'),
	    'promo_id' => set_value('promo_id'),
	    'created_datetime' => set_value('created_datetime'),
	    'updated_datetime' => set_value('updated_datetime'),
	    'created_by' => set_value('created_by'),
        'updated_by' => set_value('updated_by'),
        'rarities' => $this->Rarities_model->get_all()
    );
    
        $this->load->view('products/products_form', $data);
    }
    
    public function create_action() 
    {
        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            if($this->Products_model->is_duplicate_check($this->input->post('card_reference_id',TRUE))) {
                $this->session->set_flashdata('message', 'Product with same card reference id already exist');
                redirect(site_url('products/create'));
            }

            $data = array(
		'card_reference_id' => $this->input->post('card_reference_id',TRUE),
		'currency' => $this->input->post('currency',TRUE),
		'price' => $this->input->post('price',TRUE),
		'qty' => $this->input->post('qty',TRUE),
		'promo_id' => NULL,
		'created_datetime' => date("Y-m-d H:i:s"),
		'updated_datetime' => date("Y-m-d H:i:s"),
		'created_by' => 1,
		'updated_by' => 1,
	    );

            $this->Products_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('products'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Products_model->get_by_id($id);
        $this->load->model('Rarities_model');

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('products/update_action'),
		'id' => set_value('id', $row->product_id),
		'card_reference_id' => set_value('card_reference_id', $row->card_reference_id),
		'sku' => set_value('sku', $row->sku),
		'rarity' => set_value('rarity', $row->rarity),
		'currency' => set_value('currency', $row->currency),
		'price' => set_value('price', $row->price),
		'qty' => set_value('qty', $row->qty),
		'promo_id' => set_value('promo_id', $row->promo_id),
		'created_datetime' => date("Y-m-d H:i:s"),
		'updated_datetime' => date("Y-m-d H:i:s"),
		'created_by' => 1,
        'updated_by' => 1,
        'rarities' => $this->Rarities_model->get_all()
	    );
            $this->load->view('products/products_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('products'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'card_reference_id' => $this->input->post('card_reference_id',TRUE),
		'currency' => $this->input->post('currency',TRUE),
		'price' => $this->input->post('price',TRUE),
		'qty' => $this->input->post('qty',TRUE),
		'promo_id' => NULL,
		'created_datetime' => date("Y-m-d H:i:s"),
		'updated_datetime' => date("Y-m-d H:i:s"),
		'created_by' => 1,
		'updated_by' => 1,
	    );

            $this->Products_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('products'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Products_model->get_by_id($id);

        if ($row) {
            $this->Products_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('products'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('products'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('card_reference_id', 'card id', 'trim|required');
	$this->form_validation->set_rules('currency', 'currency', 'trim|required');
	$this->form_validation->set_rules('price', 'price', 'trim|required|numeric');
	$this->form_validation->set_rules('qty', 'qty', 'trim|required');
	$this->form_validation->set_rules('promo_id', 'promo id', 'trim|numeric');
	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "products.xls";
        $judul = "products";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Card Reference Id");
	xlsWriteLabel($tablehead, $kolomhead++, "SKU");
	xlsWriteLabel($tablehead, $kolomhead++, "Rarity");
	xlsWriteLabel($tablehead, $kolomhead++, "Currency");
	xlsWriteLabel($tablehead, $kolomhead++, "Price");
	xlsWriteLabel($tablehead, $kolomhead++, "Qty");
	xlsWriteLabel($tablehead, $kolomhead++, "Promo Id");
	xlsWriteLabel($tablehead, $kolomhead++, "Created Datetime");
	xlsWriteLabel($tablehead, $kolomhead++, "Updated Datetime");
	xlsWriteLabel($tablehead, $kolomhead++, "Created By");
	xlsWriteLabel($tablehead, $kolomhead++, "Updated By");

	foreach ($this->Products_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->card_reference_id);
	    xlsWriteNumber($tablebody, $kolombody++, $data->sku);
	    xlsWriteNumber($tablebody, $kolombody++, $data->rarity);
	    xlsWriteLabel($tablebody, $kolombody++, $data->currency);
	    xlsWriteNumber($tablebody, $kolombody++, $data->price);
	    xlsWriteNumber($tablebody, $kolombody++, $data->qty);
	    xlsWriteNumber($tablebody, $kolombody++, $data->promo_id);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created_datetime);
	    xlsWriteLabel($tablebody, $kolombody++, $data->updated_datetime);
	    xlsWriteNumber($tablebody, $kolombody++, $data->created_by);
	    xlsWriteNumber($tablebody, $kolombody++, $data->updated_by);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=products.doc");

        $data = array(
            'products_data' => $this->Products_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('products/products_doc',$data);
    }

    public function excel_tokopedia(){
		$generated_excel = $this->Generated_excel_model->find_top();
		$datetime = '2010-01-01 00:00:00';
		if(isset($generated_excel['created_datetime'])){
			$datetime = $generated_excel['created_datetime'];
		}

        $sql = "SELECT *, p.price as product_price FROM products p
        JOIN cards c ON p.card_reference_id = c.reference_id
        WHERE qty > 0 AND p.updated_datetime >= ?";
        $datas = $this->db->query($sql, array($datetime))->result_array();

        $count_datas = count($datas);

        $output = 'Error Message;Nama Produk;Deskripsi;Kategori Produk;Berat dalam Gram;Jumlah Minimum Pemesanan;Etalase;Hari Preorder;Kondisi Produk;Gambar 1 (URL);Gambar 2 (URL);Gambar 3 (URL);Gambar 4 (URL);Gambar 5 (URL);Video 1 (URL);Video 2 (URL);Video 3 (URL);SKU;Status;Stok;Harga;Wajib Asuransi (kosongin aja);Warna;Varian';
        $output .= "\r\n";
        for($a = 0; $a < $count_datas; $a++) {
            $data = $datas[$a];
            $metadata = json_decode($data['metadata_json'], true);
            $big_image = $metadata['card_images'][0]['image_url'];

            $output .= ";";
            $output .= "Yugioh ".$data['name']."(".$data['sku'].");";
            $output .= "Kondisi near mint Original TCG gambar hanyalah contoh. Kode booster sesuai dengan judul.".$data['name']." - ".$data['sku']." - ".$data['rarity'].";";
            $output .= "3197;";
            $output .= "10;";
            $output .= "1;";
            $output .= "7840431;";
            $output .= ";";
            $output .= "Baru;";
            $output .= $big_image.";";
            $output .= ";";
            $output .= ";";
            $output .= ";";
            $output .= ";";
            $output .= ";";
            $output .= ";";
            $output .= ";";
            $output .= $data['card_reference_id'].";";
            $output .= "Aktif;";
            $output .= $data['qty'].";";
            $output .= $data['product_price'].";";
            $output .= ";";
            $output .= ";";
            $output .= "\r\n";
        }

		$period = date('Y-m-d');

        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . "generated_products_tokopedia_".$period.".csv" . "");
        header("Content-Transfer-Encoding: binary ");

		$data = array(
			'filename' => 'generated_products_tokopedia.csv',
			'period' => $period,
			'created_datetime' => date("Y-m-d H:i:s")
		);
		$this->Generated_excel_model->insert($data);

        echo $output;
    }

    public function search_autocomplete() {
        $keyword = $this->input->get('keyword');
        $result = $this->Products_model->find_by_keyword($keyword);

        $outputs = array();
        foreach($result as $r) {
            $output['id'] = $r['reference_id'];
            $output['sku'] = $r['sku'];
            $output['rarity'] = $r['rarity'];
            $output['name'] = "(".$r['sku']." - ".$r['rarity'].") ".$r['name'];
            $outputs[] = $output;
        }

        echo json_encode($outputs);
    }

}

/* End of file Products.php */
/* Location: ./application/controllers/Products.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-10-10 07:14:03 */
/* http://harviacode.com */
