<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Packs extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('Packs_model');
        $this->load->library('form_validation');

        if(!$this->session->userdata('logined') || $this->session->userdata('logined') != true)
        {
            redirect('/');
        }        
	$this->load->library('datatables');
    }

    public function index()
    {
        $this->load->view('packs/packs_list');
    } 
    
    public function json() {
        header('Content-Type: application/json');
    echo $this->Packs_model->json();
    }

    public function read($id) 
    {
        $row = $this->Packs_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'pack_code' => $row->pack_code,
		'pack_name' => $row->pack_name,
		'num_of_cards' => $row->num_of_cards,
		'release_date' => $row->release_date,
		'pack_image' => $row->pack_image,
		'created_datetime' => $row->created_datetime,
		'updated_datetime' => $row->updated_datetime,
		'created_by' => $row->created_by,
		'updated_by' => $row->updated_by,
	    );
            $this->load->view('packs/packs_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('packs'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('packs/create_action'),
	    'id' => set_value('id'),
	    'pack_code' => set_value('pack_code'),
	    'pack_name' => set_value('pack_name'),
	    'num_of_cards' => set_value('num_of_cards'),
	    'release_date' => set_value('release_date'),
	    'pack_image' => set_value('pack_image'),
	    'created_datetime' => set_value('created_datetime'),
	    'updated_datetime' => set_value('updated_datetime'),
	    'created_by' => set_value('created_by'),
	    'updated_by' => set_value('updated_by'),
	);
        $this->load->view('packs/packs_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'pack_code' => $this->input->post('pack_code',TRUE),
		'pack_name' => $this->input->post('pack_name',TRUE),
		'num_of_cards' => $this->input->post('num_of_cards',TRUE),
		'release_date' => $this->input->post('release_date',TRUE),
		'pack_image' => $this->input->post('pack_image',TRUE),
		'created_datetime' => $this->input->post('created_datetime',TRUE),
		'updated_datetime' => $this->input->post('updated_datetime',TRUE),
		'created_by' => $this->input->post('created_by',TRUE),
		'updated_by' => $this->input->post('updated_by',TRUE),
	    );

            $this->Packs_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('packs'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Packs_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('packs/update_action'),
		'id' => set_value('id', $row->id),
		'pack_code' => set_value('pack_code', $row->pack_code),
		'pack_name' => set_value('pack_name', $row->pack_name),
		'num_of_cards' => set_value('num_of_cards', $row->num_of_cards),
		'release_date' => set_value('release_date', $row->release_date),
		'pack_image' => set_value('pack_image', $row->pack_image),
		'created_datetime' => set_value('created_datetime', $row->created_datetime),
		'updated_datetime' => set_value('updated_datetime', $row->updated_datetime),
		'created_by' => set_value('created_by', $row->created_by),
		'updated_by' => set_value('updated_by', $row->updated_by),
	    );
            $this->load->view('packs/packs_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('packs'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'pack_code' => $this->input->post('pack_code',TRUE),
		'pack_name' => $this->input->post('pack_name',TRUE),
		'num_of_cards' => $this->input->post('num_of_cards',TRUE),
		'release_date' => $this->input->post('release_date',TRUE),
		'pack_image' => $this->input->post('pack_image',TRUE),
		'created_datetime' => $this->input->post('created_datetime',TRUE),
		'updated_datetime' => $this->input->post('updated_datetime',TRUE),
		'created_by' => $this->input->post('created_by',TRUE),
		'updated_by' => $this->input->post('updated_by',TRUE),
	    );

            $this->Packs_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('packs'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Packs_model->get_by_id($id);

        if ($row) {
            $this->Packs_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('packs'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('packs'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('pack_code', 'pack code', 'trim|required');
	$this->form_validation->set_rules('pack_name', 'pack name', 'trim|required');
	$this->form_validation->set_rules('num_of_cards', 'num of cards', 'trim|required');
	$this->form_validation->set_rules('release_date', 'release date', 'trim|required');
	$this->form_validation->set_rules('pack_image', 'pack image', 'trim|required');
	$this->form_validation->set_rules('created_datetime', 'created datetime', 'trim|required');
	$this->form_validation->set_rules('updated_datetime', 'updated datetime', 'trim|required');
	$this->form_validation->set_rules('created_by', 'created by', 'trim|required');
	$this->form_validation->set_rules('updated_by', 'updated by', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "packs.xls";
        $judul = "packs";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Pack Code");
	xlsWriteLabel($tablehead, $kolomhead++, "Pack Name");
	xlsWriteLabel($tablehead, $kolomhead++, "Num Of Cards");
	xlsWriteLabel($tablehead, $kolomhead++, "Release Date");
	xlsWriteLabel($tablehead, $kolomhead++, "Pack Image");
	xlsWriteLabel($tablehead, $kolomhead++, "Created Datetime");
	xlsWriteLabel($tablehead, $kolomhead++, "Updated Datetime");
	xlsWriteLabel($tablehead, $kolomhead++, "Created By");
	xlsWriteLabel($tablehead, $kolomhead++, "Updated By");

	foreach ($this->Packs_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->pack_code);
	    xlsWriteLabel($tablebody, $kolombody++, $data->pack_name);
	    xlsWriteNumber($tablebody, $kolombody++, $data->num_of_cards);
	    xlsWriteLabel($tablebody, $kolombody++, $data->release_date);
	    xlsWriteLabel($tablebody, $kolombody++, $data->pack_image);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created_datetime);
	    xlsWriteLabel($tablebody, $kolombody++, $data->updated_datetime);
	    xlsWriteNumber($tablebody, $kolombody++, $data->created_by);
	    xlsWriteNumber($tablebody, $kolombody++, $data->updated_by);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=packs.doc");

        $data = array(
            'packs_data' => $this->Packs_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('packs/packs_doc',$data);
    }

}

/* End of file Packs.php */
/* Location: ./application/controllers/Packs.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2023-05-16 18:50:59 */
/* http://harviacode.com */