<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Transaction_details extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('Transaction_details_model');
        $this->load->library('form_validation');

        if(!$this->session->userdata('logined') || $this->session->userdata('logined') != true)
        {
            redirect('/');
        }        
	$this->load->library('datatables');
    }

    public function index()
    {
        $this->load->view('transaction_details/transaction_details_list');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Transaction_details_model->json();
    }

    public function read($id) 
    {
        $row = $this->Transaction_details_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'transaction_id' => $row->transaction_id,
		'product_id' => $row->product_id,
		'qty' => $row->qty,
		'price' => $row->price,
		'subtotal' => $row->subtotal,
	    );
            $this->load->view('transaction_details/transaction_details_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('transaction_details'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('transaction_details/create_action'),
	    'id' => set_value('id'),
	    'transaction_id' => set_value('transaction_id'),
	    'product_id' => set_value('product_id'),
	    'qty' => set_value('qty'),
	    'price' => set_value('price'),
	    'subtotal' => set_value('subtotal'),
	);
        $this->load->view('transaction_details/transaction_details_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'transaction_id' => $this->input->post('transaction_id',TRUE),
		'product_id' => $this->input->post('product_id',TRUE),
		'qty' => $this->input->post('qty',TRUE),
		'price' => $this->input->post('price',TRUE),
		'subtotal' => $this->input->post('subtotal',TRUE),
	    );

            $this->Transaction_details_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('transaction_details'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Transaction_details_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('transaction_details/update_action'),
		'id' => set_value('id', $row->id),
		'transaction_id' => set_value('transaction_id', $row->transaction_id),
		'product_id' => set_value('product_id', $row->product_id),
		'qty' => set_value('qty', $row->qty),
		'price' => set_value('price', $row->price),
		'subtotal' => set_value('subtotal', $row->subtotal),
	    );
            $this->load->view('transaction_details/transaction_details_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('transaction_details'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'transaction_id' => $this->input->post('transaction_id',TRUE),
		'product_id' => $this->input->post('product_id',TRUE),
		'qty' => $this->input->post('qty',TRUE),
		'price' => $this->input->post('price',TRUE),
		'subtotal' => $this->input->post('subtotal',TRUE),
	    );

            $this->Transaction_details_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('transaction_details'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Transaction_details_model->get_by_id($id);

        if ($row) {
            $this->Transaction_details_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('transaction_details'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('transaction_details'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('transaction_id', 'transaction id', 'trim|required');
	$this->form_validation->set_rules('product_id', 'product id', 'trim|required');
	$this->form_validation->set_rules('qty', 'qty', 'trim|required');
	$this->form_validation->set_rules('price', 'price', 'trim|required|numeric');
	$this->form_validation->set_rules('subtotal', 'subtotal', 'trim|required|numeric');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "transaction_details.xls";
        $judul = "transaction_details";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Transaction Id");
	xlsWriteLabel($tablehead, $kolomhead++, "Product Id");
	xlsWriteLabel($tablehead, $kolomhead++, "Qty");
	xlsWriteLabel($tablehead, $kolomhead++, "Price");
	xlsWriteLabel($tablehead, $kolomhead++, "Subtotal");

	foreach ($this->Transaction_details_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->transaction_id);
	    xlsWriteNumber($tablebody, $kolombody++, $data->product_id);
	    xlsWriteNumber($tablebody, $kolombody++, $data->qty);
	    xlsWriteNumber($tablebody, $kolombody++, $data->price);
	    xlsWriteNumber($tablebody, $kolombody++, $data->subtotal);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=transaction_details.doc");

        $data = array(
            'transaction_details_data' => $this->Transaction_details_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('transaction_details/transaction_details_doc',$data);
    }

}

/* End of file Transaction_details.php */
/* Location: ./application/controllers/Transaction_details.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-11-22 11:01:49 */
/* http://harviacode.com */