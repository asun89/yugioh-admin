<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Transaction_payments extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('Transaction_payments_model');
        $this->load->library('form_validation');

        if(!$this->session->userdata('logined') || $this->session->userdata('logined') != true)
        {
            redirect('/');
        }        
	$this->load->library('datatables');
    }

    public function index()
    {
        $this->load->view('transaction_payments/transaction_payments_list');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Transaction_payments_model->json();
    }

    public function read($id) 
    {
        $row = $this->Transaction_payments_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'transaction_id' => $row->transaction_id,
		'payment_method_id' => $row->payment_method_id,
		'account_no' => $row->account_no,
		'account_owner' => $row->account_owner,
		'amount' => $row->amount,
		'status' => $row->status,
		'created_datetime' => $row->created_datetime,
		'updated_datetime' => $row->updated_datetime,
		'created_by' => $row->created_by,
		'updated_by' => $row->updated_by,
	    );
            $this->load->view('transaction_payments/transaction_payments_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('transaction_payments'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('transaction_payments/create_action'),
	    'id' => set_value('id'),
	    'transaction_id' => set_value('transaction_id'),
	    'payment_method_id' => set_value('payment_method_id'),
	    'account_no' => set_value('account_no'),
	    'account_owner' => set_value('account_owner'),
	    'amount' => set_value('amount'),
	    'status' => set_value('status'),
	    'created_datetime' => set_value('created_datetime'),
	    'updated_datetime' => set_value('updated_datetime'),
	    'created_by' => set_value('created_by'),
	    'updated_by' => set_value('updated_by'),
	);
        $this->load->view('transaction_payments/transaction_payments_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'transaction_id' => $this->input->post('transaction_id',TRUE),
		'payment_method_id' => $this->input->post('payment_method_id',TRUE),
		'account_no' => $this->input->post('account_no',TRUE),
		'account_owner' => $this->input->post('account_owner',TRUE),
		'amount' => $this->input->post('amount',TRUE),
		'status' => $this->input->post('status',TRUE),
		'created_datetime' => $this->input->post('created_datetime',TRUE),
		'updated_datetime' => $this->input->post('updated_datetime',TRUE),
		'created_by' => $this->input->post('created_by',TRUE),
		'updated_by' => $this->input->post('updated_by',TRUE),
	    );

            $this->Transaction_payments_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('transaction_payments'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Transaction_payments_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('transaction_payments/update_action'),
		'id' => set_value('id', $row->id),
		'transaction_id' => set_value('transaction_id', $row->transaction_id),
		'payment_method_id' => set_value('payment_method_id', $row->payment_method_id),
		'account_no' => set_value('account_no', $row->account_no),
		'account_owner' => set_value('account_owner', $row->account_owner),
		'amount' => set_value('amount', $row->amount),
		'status' => set_value('status', $row->status),
		'created_datetime' => set_value('created_datetime', $row->created_datetime),
		'updated_datetime' => set_value('updated_datetime', $row->updated_datetime),
		'created_by' => set_value('created_by', $row->created_by),
		'updated_by' => set_value('updated_by', $row->updated_by),
	    );
            $this->load->view('transaction_payments/transaction_payments_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('transaction_payments'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'transaction_id' => $this->input->post('transaction_id',TRUE),
		'payment_method_id' => $this->input->post('payment_method_id',TRUE),
		'account_no' => $this->input->post('account_no',TRUE),
		'account_owner' => $this->input->post('account_owner',TRUE),
		'amount' => $this->input->post('amount',TRUE),
		'status' => $this->input->post('status',TRUE),
		'created_datetime' => $this->input->post('created_datetime',TRUE),
		'updated_datetime' => $this->input->post('updated_datetime',TRUE),
		'created_by' => $this->input->post('created_by',TRUE),
		'updated_by' => $this->input->post('updated_by',TRUE),
	    );

            $this->Transaction_payments_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('transaction_payments'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Transaction_payments_model->get_by_id($id);

        if ($row) {
            $this->Transaction_payments_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('transaction_payments'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('transaction_payments'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('transaction_id', 'transaction id', 'trim|required');
	$this->form_validation->set_rules('payment_method_id', 'payment method id', 'trim|required');
	$this->form_validation->set_rules('account_no', 'account no', 'trim|required');
	$this->form_validation->set_rules('account_owner', 'account owner', 'trim|required');
	$this->form_validation->set_rules('amount', 'amount', 'trim|required|numeric');
	$this->form_validation->set_rules('status', 'status', 'trim|required');
	$this->form_validation->set_rules('created_datetime', 'created datetime', 'trim|required');
	$this->form_validation->set_rules('updated_datetime', 'updated datetime', 'trim|required');
	$this->form_validation->set_rules('created_by', 'created by', 'trim|required');
	$this->form_validation->set_rules('updated_by', 'updated by', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "transaction_payments.xls";
        $judul = "transaction_payments";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Transaction Id");
	xlsWriteLabel($tablehead, $kolomhead++, "Payment Method Id");
	xlsWriteLabel($tablehead, $kolomhead++, "Account No");
	xlsWriteLabel($tablehead, $kolomhead++, "Account Owner");
	xlsWriteLabel($tablehead, $kolomhead++, "Amount");
	xlsWriteLabel($tablehead, $kolomhead++, "Status");
	xlsWriteLabel($tablehead, $kolomhead++, "Created Datetime");
	xlsWriteLabel($tablehead, $kolomhead++, "Updated Datetime");
	xlsWriteLabel($tablehead, $kolomhead++, "Created By");
	xlsWriteLabel($tablehead, $kolomhead++, "Updated By");

	foreach ($this->Transaction_payments_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->transaction_id);
	    xlsWriteNumber($tablebody, $kolombody++, $data->payment_method_id);
	    xlsWriteLabel($tablebody, $kolombody++, $data->account_no);
	    xlsWriteLabel($tablebody, $kolombody++, $data->account_owner);
	    xlsWriteNumber($tablebody, $kolombody++, $data->amount);
	    xlsWriteLabel($tablebody, $kolombody++, $data->status);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created_datetime);
	    xlsWriteLabel($tablebody, $kolombody++, $data->updated_datetime);
	    xlsWriteNumber($tablebody, $kolombody++, $data->created_by);
	    xlsWriteNumber($tablebody, $kolombody++, $data->updated_by);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=transaction_payments.doc");

        $data = array(
            'transaction_payments_data' => $this->Transaction_payments_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('transaction_payments/transaction_payments_doc',$data);
    }

}

/* End of file Transaction_payments.php */
/* Location: ./application/controllers/Transaction_payments.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-11-22 11:01:53 */
/* http://harviacode.com */