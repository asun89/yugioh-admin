<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Currency extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('Currency_model');
        $this->load->library('form_validation');

        if(!$this->session->userdata('logined') || $this->session->userdata('logined') != true)
        {
            redirect('/');
        }        
	$this->load->library('datatables');
    }

    public function index()
    {
        $this->load->view('currency/currency_list');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Currency_model->json();
    }

    public function read($id) 
    {
        $row = $this->Currency_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'currency_from' => $row->currency_from,
		'currency_to' => $row->currency_to,
		'buy_price' => $row->buy_price,
		'sell_price' => $row->sell_price,
		'created_datetime' => $row->created_datetime,
		'created_by' => $row->created_by,
		'updated_datetime' => $row->updated_datetime,
		'updated_by' => $row->updated_by,
	    );
            $this->load->view('currency/currency_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('currency'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('currency/create_action'),
	    'id' => set_value('id'),
	    'currency_from' => set_value('currency_from'),
	    'currency_to' => set_value('currency_to'),
	    'buy_price' => set_value('buy_price'),
	    'sell_price' => set_value('sell_price'),
	    'created_datetime' => set_value('created_datetime'),
	    'created_by' => set_value('created_by'),
	    'updated_datetime' => set_value('updated_datetime'),
	    'updated_by' => set_value('updated_by'),
	);
        $this->load->view('currency/currency_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'currency_from' => $this->input->post('currency_from',TRUE),
		'currency_to' => $this->input->post('currency_to',TRUE),
		'buy_price' => $this->input->post('buy_price',TRUE),
		'sell_price' => $this->input->post('sell_price',TRUE),
		'created_datetime' => $this->input->post('created_datetime',TRUE),
		'created_by' => $this->input->post('created_by',TRUE),
		'updated_datetime' => $this->input->post('updated_datetime',TRUE),
		'updated_by' => $this->input->post('updated_by',TRUE),
	    );

            $this->Currency_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('currency'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Currency_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('currency/update_action'),
		'id' => set_value('id', $row->id),
		'currency_from' => set_value('currency_from', $row->currency_from),
		'currency_to' => set_value('currency_to', $row->currency_to),
		'buy_price' => set_value('buy_price', $row->buy_price),
		'sell_price' => set_value('sell_price', $row->sell_price),
		'created_datetime' => set_value('created_datetime', $row->created_datetime),
		'created_by' => set_value('created_by', $row->created_by),
		'updated_datetime' => set_value('updated_datetime', $row->updated_datetime),
		'updated_by' => set_value('updated_by', $row->updated_by),
	    );
            $this->load->view('currency/currency_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('currency'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'currency_from' => $this->input->post('currency_from',TRUE),
		'currency_to' => $this->input->post('currency_to',TRUE),
		'buy_price' => $this->input->post('buy_price',TRUE),
		'sell_price' => $this->input->post('sell_price',TRUE),
		'created_datetime' => $this->input->post('created_datetime',TRUE),
		'created_by' => $this->input->post('created_by',TRUE),
		'updated_datetime' => $this->input->post('updated_datetime',TRUE),
		'updated_by' => $this->input->post('updated_by',TRUE),
	    );

            $this->Currency_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('currency'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Currency_model->get_by_id($id);

        if ($row) {
            $this->Currency_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('currency'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('currency'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('currency_from', 'currency from', 'trim|required');
	$this->form_validation->set_rules('currency_to', 'currency to', 'trim|required');
	$this->form_validation->set_rules('buy_price', 'buy price', 'trim|required');
	$this->form_validation->set_rules('sell_price', 'sell price', 'trim|required');
	$this->form_validation->set_rules('created_datetime', 'created datetime', 'trim|required');
	$this->form_validation->set_rules('created_by', 'created by', 'trim|required');
	$this->form_validation->set_rules('updated_datetime', 'updated datetime', 'trim|required');
	$this->form_validation->set_rules('updated_by', 'updated by', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "currency.xls";
        $judul = "currency";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Currency From");
	xlsWriteLabel($tablehead, $kolomhead++, "Currency To");
	xlsWriteLabel($tablehead, $kolomhead++, "Buy Price");
	xlsWriteLabel($tablehead, $kolomhead++, "Sell Price");
	xlsWriteLabel($tablehead, $kolomhead++, "Created Datetime");
	xlsWriteLabel($tablehead, $kolomhead++, "Created By");
	xlsWriteLabel($tablehead, $kolomhead++, "Updated Datetime");
	xlsWriteLabel($tablehead, $kolomhead++, "Updated By");

	foreach ($this->Currency_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->currency_from);
	    xlsWriteLabel($tablebody, $kolombody++, $data->currency_to);
	    xlsWriteLabel($tablebody, $kolombody++, $data->buy_price);
	    xlsWriteLabel($tablebody, $kolombody++, $data->sell_price);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created_datetime);
	    xlsWriteNumber($tablebody, $kolombody++, $data->created_by);
	    xlsWriteLabel($tablebody, $kolombody++, $data->updated_datetime);
	    xlsWriteNumber($tablebody, $kolombody++, $data->updated_by);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=currency.doc");

        $data = array(
            'currency_data' => $this->Currency_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('currency/currency_doc',$data);
    }

}

/* End of file Currency.php */
/* Location: ./application/controllers/Currency.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-10-10 07:14:03 */
/* http://harviacode.com */