<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Scrapper extends CI_Controller {
    function __construct()
    {
        parent::__construct();      
    }

	public function scrap_packs(){
		$input_force = $this->input->get('force');
		$force = false;
		if(isset($input_force) && $input_force == "true") {
			$force = true;
		}

		$this->load->model('scrapping_model');
		$response = $this->scrapping_model->get_all_ygoprodeck_packs_data($force);
		if(!$response) {
			echo "No update for the current database";
			return true;
		}

		$data = $response;
		$data_size = count($data);
		if($data_size == 0){
			return false;
		} 
		$this->scrapping_model->delete_packs();
		for($a = 0; $a < $data_size; $a++){
			$pack_data = $data[$a];
			$pack = array();
			$pack['pack_code'] = $pack_data['set_code'];
			$pack['pack_name'] = $pack_data['set_name'];
			$pack['num_of_cards'] = $pack_data['num_of_cards'];
			$pack['release_date'] = isset($pack_data['tcg_date']) ? $pack_data['tcg_date'] : date("Y-m-d");
			$pack['pack_image'] = isset($pack_data['set_image']) ? $pack_data['set_image'] : "";
			$pack['created_by'] = 1;
			$pack['updated_by'] = 1;
			
			$this->scrapping_model->insert_new_packs($pack);
		}
		echo $data_size ." pack(s) data inserted successfully";

	}

    public function scrap($cred){
		if($cred != LOGIN_USERNAME) {
			return false;
		}

		$input_force = $this->input->get('force');
		$force = false;
		if(isset($input_force) && $input_force == "true") {
			$force = true;
		}

		$this->load->model('scrapping_model');
		set_time_limit(3600);
		ini_set('memory_limit', -1);
		
		$response = $this->scrapping_model->get_all_ygoprodeck_data($force);

		if(!$response) {
			echo "No update for the current database";
			return true;
		}

		$data = $response['data'];
		$data_size = count($data);
		if($data_size == 0){
			return false;
		} 
		$this->scrapping_model->delete_cards();
		for($a = 0; $a < $data_size; $a++){
			$card_data = $data[$a];
			if(isset($card_data['card_sets'])){
				foreach($card_data['card_sets'] as $set){
					$card = array();
					$card['id'] = $card_data['id'];
					$card['sku'] = $set['set_code'];
					$card['pack_code'] = explode("-",$card['sku'])[0];
					$card['name'] = $card_data['name'];
					$card['rarity'] = $set['set_rarity'];
					$card['currency'] = "IDR";
					$card['original_currency'] = "IDR";
					$card['reference_id'] = $card_data['id']."-".$set['set_code']."-".str_replace(" ", "_", strtoupper($set['set_rarity']));
					$card['external_reference_id'] = $card_data['id'];
					$card['metadata_json'] = json_encode($card_data);
					$this->scrapping_model->insert_new_cards($card);
				}
			}
		}
		echo $data_size ." card(s) data inserted successfully";
	}

	public function initiate_price(){
		$this->load->model('scrapping_model');

		$rate = $this->scrapping_model->get_currency_rate();

		$cards = $this->scrapping_model->get_cards_without_price();
		$count_cards = count($cards);
		
		for($a = 0; $a < $count_cards; $a++){
			$sku = $cards[$a]['sku'];
			$card_reference_id = $cards[$a]['card_reference_id'];
			$sku_rarity = urlencode($cards[$a]['sku']." ".$cards[$a]['rarity']);
			$price = $this->scrapping_model->get_card_price_by_sku_from_tnt($sku_rarity);

			$total_price = ($price * $rate);
			$this->scrapping_model->update_card_price($sku, $price, $total_price);
			$this->scrapping_model->update_product_price($card_reference_id, $total_price);
		}
    }
}
