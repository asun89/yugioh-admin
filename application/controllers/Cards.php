<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cards extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('Cards_model');
        $this->load->library('form_validation');

        if(!$this->session->userdata('logined') || $this->session->userdata('logined') != true)
        {
            redirect('/');
        }        
	$this->load->library('datatables');
    }

    public function index()
    {
        $this->load->view('cards/cards_list');
    } 
    
    public function get_card_data(){
        $sku = $this->input->get('sku');
        $rarity = $this->input->get('rarity');

        if(empty($sku)){
            echo 'false';
            return false;
        }

        if(empty($rarity)){
            $rarity = 'Common';
        }
		$this->load->model('currency_model');
        $row = $this->Cards_model->get_by_sku_rarity($sku, $rarity);
        if(!$row){
            echo 'false';
            return false;
        }
        $rate = $this->currency_model->get_currency_rate();
        $price = $row->price;
        if($row->price <= 0){
            $price = $this->get_card_price_by_sku_from_tnt(urlencode($row->sku." ".$row->rarity));
            $row->original_price = $price;
            $row->price = $price * $rate;

            $data = array(
                'button' => 'Update',
                'action' => site_url('cards/update_action'),
                'id' => set_value('id', $row->id),
                'name' => set_value('name', $row->name),
                'sku' => set_value('sku', $row->sku),
                'rarity' => set_value('rarity', $row->rarity),
                'reference_id' => set_value('reference_id', $row->reference_id),
                'original_currency' => set_value('original_currency', $row->original_currency),
                'original_price' => set_value('original_price', $row->original_price),
                'currency' => set_value('currency', $row->currency),
                'price' => set_value('price', $row->price),
                'metadata_json' => set_value('metadata_json', $row->metadata_json),
                'created_datetime' => set_value('created_datetime', $row->created_datetime),
                'updated_datetime' => set_value('updated_datetime', $row->updated_datetime),
                'created_by' => set_value('created_by', $row->created_by),
                'updated_by' => set_value('updated_by', $row->updated_by),
            );
            $this->Cards_model->update($row->id, $row);
        }
        $row->currency = "IDR";
        
        echo json_encode($row);
    }

    public function get_card_price_by_sku_from_tnt($sku_rarity){
        try
        {
            $this->load->helper("simple_html_dom");
            $yugioh_page = file_get_contents("https://www.trollandtoad.com");
            preg_match_all('/name=\'token\' value=\'(.*)\'>/', $yugioh_page, $output_array);
            $html = file_get_html("https://www.trollandtoad.com/category.php?token=".$output_array[1][0]."&search-words=".$sku_rarity."&selected-cat=0");
            $output = "";
            foreach($html->find('.buying-options-table') as $element){
                $output = trim(preg_replace("/[\\n\\r]+/", "", $element->plaintext));
                break;
            }

            $data = explode("$", $output);
            $price_data = explode(" ", $data[1]);
            
            return $price_data[0];
        }
        catch (Throwable $t)
        {
            return 0;
        }
    }

    public function json() {
		ini_set('memory_limit', '256M');
        header('Content-Type: application/json');
        echo $this->Cards_model->json();
    }

    public function read($id) 
    {
        $row = $this->Cards_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'name' => $row->name,
		'sku' => $row->sku,
		'rarity' => $row->rarity,
		'reference_id' => $row->reference_id,
		'external_reference_id' => $row->external_reference_id,
		'original_currency' => $row->original_currency,
		'original_price' => $row->original_price,
		'currency' => $row->currency,
		'price' => $row->price,
		'metadata_json' => $row->metadata_json,
		'created_datetime' => $row->created_datetime,
		'updated_datetime' => $row->updated_datetime,
		'created_by' => $row->created_by,
		'updated_by' => $row->updated_by,
	    );
            $this->load->view('cards/cards_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('cards'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('cards/create_action'),
	    'id' => set_value('id'),
	    'name' => set_value('name'),
	    'sku' => set_value('sku'),
	    'rarity' => set_value('rarity'),
	    'reference_id' => set_value('reference_id'),
	    'original_currency' => set_value('original_currency'),
	    'original_price' => set_value('original_price'),
	    'currency' => set_value('currency'),
	    'price' => set_value('price'),
	    'metadata_json' => set_value('metadata_json'),
	    'created_datetime' => set_value('created_datetime'),
	    'updated_datetime' => set_value('updated_datetime'),
	    'created_by' => set_value('created_by'),
	    'updated_by' => set_value('updated_by'),
	);
        $this->load->view('cards/cards_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'name' => $this->input->post('name',TRUE),
		'sku' => $this->input->post('sku',TRUE),
		'rarity' => $this->input->post('rarity',TRUE),
		'reference_id' => $this->input->post('reference_id',TRUE),
		'original_currency' => $this->input->post('original_currency',TRUE),
		'original_price' => $this->input->post('original_price',TRUE),
		'currency' => $this->input->post('currency',TRUE),
		'price' => $this->input->post('price',TRUE),
		'metadata_json' => $this->input->post('metadata_json',TRUE),
		'created_datetime' => $this->input->post('created_datetime',TRUE),
		'updated_datetime' => $this->input->post('updated_datetime',TRUE),
		'created_by' => $this->input->post('created_by',TRUE),
		'updated_by' => $this->input->post('updated_by',TRUE),
	    );

            $this->Cards_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('cards'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Cards_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('cards/update_action'),
		'id' => set_value('id', $row->id),
		'name' => set_value('name', $row->name),
		'sku' => set_value('sku', $row->sku),
		'rarity' => set_value('rarity', $row->rarity),
		'reference_id' => set_value('reference_id', $row->reference_id),
		'external_reference_id' => set_value('external_reference_id', $row->external_reference_id),
		'original_currency' => set_value('original_currency', $row->original_currency),
		'original_price' => set_value('original_price', $row->original_price),
		'currency' => set_value('currency', $row->currency),
		'price' => set_value('price', $row->price),
		'metadata_json' => set_value('metadata_json', $row->metadata_json),
		'created_datetime' => set_value('created_datetime', $row->created_datetime),
		'updated_datetime' => set_value('updated_datetime', $row->updated_datetime),
		'created_by' => set_value('created_by', $row->created_by),
		'updated_by' => set_value('updated_by', $row->updated_by),
	    );
            $this->load->view('cards/cards_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('cards'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'name' => $this->input->post('name',TRUE),
		'sku' => $this->input->post('sku',TRUE),
		'rarity' => $this->input->post('rarity',TRUE),
		'reference_id' => $this->input->post('reference_id',TRUE),
		'external_reference_id' => $this->input->post('external_reference_id',TRUE),
		'original_currency' => $this->input->post('original_currency',TRUE),
		'original_price' => $this->input->post('original_price',TRUE),
		'currency' => $this->input->post('currency',TRUE),
		'price' => $this->input->post('price',TRUE),
		'metadata_json' => $this->input->post('metadata_json',TRUE),
		'created_datetime' => $this->input->post('created_datetime',TRUE),
		'updated_datetime' => $this->input->post('updated_datetime',TRUE),
		'created_by' => $this->input->post('created_by',TRUE),
		'updated_by' => $this->input->post('updated_by',TRUE),
	    );

            $this->Cards_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('cards'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Cards_model->get_by_id($id);

        if ($row) {
            $this->Cards_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('cards'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('cards'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('name', 'name', 'trim|required');
	$this->form_validation->set_rules('sku', 'sku', 'trim|required');
	$this->form_validation->set_rules('rarity', 'rarity', 'trim|required');
	$this->form_validation->set_rules('reference_id', 'reference id', 'trim|required');
	$this->form_validation->set_rules('external_reference_id', 'external_reference_id id', 'trim|required');
	$this->form_validation->set_rules('original_currency', 'original currency', 'trim|required');
	$this->form_validation->set_rules('original_price', 'original price', 'trim|required');
	$this->form_validation->set_rules('currency', 'currency', 'trim|required');
	$this->form_validation->set_rules('price', 'price', 'trim|required');
	$this->form_validation->set_rules('metadata_json', 'metadata json', 'trim|required');
	$this->form_validation->set_rules('created_datetime', 'created datetime', 'trim|required');
	$this->form_validation->set_rules('updated_datetime', 'updated datetime', 'trim|required');
	$this->form_validation->set_rules('created_by', 'created by', 'trim|required');
	$this->form_validation->set_rules('updated_by', 'updated by', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "cards.xls";
        $judul = "cards";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Name");
	xlsWriteLabel($tablehead, $kolomhead++, "Sku");
	xlsWriteLabel($tablehead, $kolomhead++, "Rarity");
	xlsWriteLabel($tablehead, $kolomhead++, "Reference Id");
	xlsWriteLabel($tablehead, $kolomhead++, "External Reference Id");
	xlsWriteLabel($tablehead, $kolomhead++, "Original Currency");
	xlsWriteLabel($tablehead, $kolomhead++, "Original Price");
	xlsWriteLabel($tablehead, $kolomhead++, "Currency");
	xlsWriteLabel($tablehead, $kolomhead++, "Price");
	xlsWriteLabel($tablehead, $kolomhead++, "Metadata Json");
	xlsWriteLabel($tablehead, $kolomhead++, "Created Datetime");
	xlsWriteLabel($tablehead, $kolomhead++, "Updated Datetime");
	xlsWriteLabel($tablehead, $kolomhead++, "Created By");
	xlsWriteLabel($tablehead, $kolomhead++, "Updated By");

	foreach ($this->Cards_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->name);
	    xlsWriteLabel($tablebody, $kolombody++, $data->sku);
	    xlsWriteLabel($tablebody, $kolombody++, $data->rarity);
	    xlsWriteLabel($tablebody, $kolombody++, $data->reference_id);
	    xlsWriteLabel($tablebody, $kolombody++, $data->external_reference_id);
	    xlsWriteLabel($tablebody, $kolombody++, $data->original_currency);
	    xlsWriteLabel($tablebody, $kolombody++, $data->original_price);
	    xlsWriteLabel($tablebody, $kolombody++, $data->currency);
	    xlsWriteLabel($tablebody, $kolombody++, $data->price);
	    xlsWriteLabel($tablebody, $kolombody++, $data->metadata_json);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created_datetime);
	    xlsWriteLabel($tablebody, $kolombody++, $data->updated_datetime);
	    xlsWriteNumber($tablebody, $kolombody++, $data->created_by);
	    xlsWriteNumber($tablebody, $kolombody++, $data->updated_by);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=cards.doc");

        $data = array(
            'cards_data' => $this->Cards_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('cards/cards_doc',$data);
    }

}

/* End of file Cards.php */
/* Location: ./application/controllers/Cards.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-10-10 07:14:03 */
/* http://harviacode.com */