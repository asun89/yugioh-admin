<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Transactions extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('Transactions_model');
		$this->load->model('Transaction_details_model');
        $this->load->library('form_validation');

        if(!$this->session->userdata('logined') || $this->session->userdata('logined') != true)
        {
            redirect('/');
        }        
	$this->load->library('datatables');
    }

    public function index()
    {
        $this->load->view('transactions/transactions_list');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Transactions_model->json();
    }

    public function read($id) 
    {
        $row = $this->Transactions_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'total_price' => $row->total_price,
		'total_discount' => $row->total_discount,
		'total_markup' => $row->total_markup,
		'account_id' => $row->account_id,
		'customer_title' => $row->customer_title,
		'customer_fullname' => $row->customer_fullname,
		'customer_email' => $row->customer_email,
		'customer_phone' => $row->customer_phone,
		'customer_address' => $row->customer_address,
		'customer_tracker_key' => $row->customer_tracker_key,
		'status' => $row->status,
		'created_datetime' => $row->created_datetime,
		'updated_datetime' => $row->updated_datetime,
		'created_by' => $row->created_by,
		'updated_by' => $row->updated_by,
	    );
            $this->load->view('transactions/transactions_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('transactions'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('transactions/create_action'),
	    'id' => set_value('id'),
	    'total_price' => set_value('total_price'),
	    'total_discount' => set_value('total_discount'),
	    'total_markup' => set_value('total_markup'),
	    'account_id' => set_value('account_id'),
	    'customer_title' => set_value('customer_title'),
	    'customer_fullname' => set_value('customer_fullname'),
	    'customer_email' => set_value('customer_email'),
	    'customer_phone' => set_value('customer_phone'),
	    'customer_address' => set_value('customer_address'),
	    'customer_tracker_key' => set_value('customer_tracker_key'),
	    'status' => set_value('status'),
	    'created_datetime' => set_value('created_datetime'),
	    'updated_datetime' => set_value('updated_datetime'),
	    'created_by' => set_value('created_by'),
	    'updated_by' => set_value('updated_by'),
	);
        $this->load->view('transactions/transactions_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'total_price' => $this->input->post('total_price',TRUE),
		'total_discount' => $this->input->post('total_discount',TRUE),
		'total_markup' => $this->input->post('total_markup',TRUE),
		'account_id' => $this->input->post('account_id',TRUE),
		'customer_title' => $this->input->post('customer_title',TRUE),
		'customer_fullname' => $this->input->post('customer_fullname',TRUE),
		'customer_email' => $this->input->post('customer_email',TRUE),
		'customer_phone' => $this->input->post('customer_phone',TRUE),
		'customer_address' => $this->input->post('customer_address',TRUE),
		'customer_tracker_key' => $this->input->post('customer_tracker_key',TRUE),
		'status' => $this->input->post('status',TRUE),
		'created_datetime' => $this->input->post('created_datetime',TRUE),
		'updated_datetime' => $this->input->post('updated_datetime',TRUE),
		'created_by' => $this->input->post('created_by',TRUE),
		'updated_by' => $this->input->post('updated_by',TRUE),
	    );

            $this->Transactions_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('transactions'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Transactions_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('transactions/update_action'),
		'id' => set_value('id', $row->id),
		'total_price' => set_value('total_price', $row->total_price),
		'total_discount' => set_value('total_discount', $row->total_discount),
		'total_markup' => set_value('total_markup', $row->total_markup),
		'account_id' => set_value('account_id', $row->account_id),
		'customer_title' => set_value('customer_title', $row->customer_title),
		'customer_fullname' => set_value('customer_fullname', $row->customer_fullname),
		'customer_email' => set_value('customer_email', $row->customer_email),
		'customer_phone' => set_value('customer_phone', $row->customer_phone),
		'customer_address' => set_value('customer_address', $row->customer_address),
		'customer_tracker_key' => set_value('customer_tracker_key', $row->customer_tracker_key),
		'status' => set_value('status', $row->status),
		'created_datetime' => set_value('created_datetime', $row->created_datetime),
		'updated_datetime' => set_value('updated_datetime', $row->updated_datetime),
		'created_by' => set_value('created_by', $row->created_by),
		'updated_by' => set_value('updated_by', $row->updated_by),
	    );
            $this->load->view('transactions/transactions_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('transactions'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'total_price' => $this->input->post('total_price',TRUE),
		'total_discount' => $this->input->post('total_discount',TRUE),
		'total_markup' => $this->input->post('total_markup',TRUE),
		'account_id' => $this->input->post('account_id',TRUE),
		'customer_title' => $this->input->post('customer_title',TRUE),
		'customer_fullname' => $this->input->post('customer_fullname',TRUE),
		'customer_email' => $this->input->post('customer_email',TRUE),
		'customer_phone' => $this->input->post('customer_phone',TRUE),
		'customer_address' => $this->input->post('customer_address',TRUE),
		'customer_tracker_key' => $this->input->post('customer_tracker_key',TRUE),
		'status' => $this->input->post('status',TRUE),
		'created_datetime' => $this->input->post('created_datetime',TRUE),
		'updated_datetime' => $this->input->post('updated_datetime',TRUE),
		'created_by' => $this->input->post('created_by',TRUE),
		'updated_by' => $this->input->post('updated_by',TRUE),
	    );

            $this->Transactions_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('transactions'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Transactions_model->get_by_id($id);

        if ($row) {
			$this->Transaction_details_model->delete_by_transaction_id($id);
            $this->Transactions_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('transactions'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('transactions'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('total_price', 'total price', 'trim|required');
	$this->form_validation->set_rules('total_discount', 'total discount', 'trim|required');
	$this->form_validation->set_rules('total_markup', 'total markup', 'trim|required');
	$this->form_validation->set_rules('account_id', 'account id', 'trim|required');
	$this->form_validation->set_rules('customer_title', 'customer title', 'trim|required');
	$this->form_validation->set_rules('customer_fullname', 'customer fullname', 'trim|required');
	$this->form_validation->set_rules('customer_email', 'customer email', 'trim|required');
	$this->form_validation->set_rules('customer_phone', 'customer phone', 'trim|required');
	$this->form_validation->set_rules('customer_address', 'customer address', 'trim|required');
	$this->form_validation->set_rules('customer_tracker_key', 'customer tracker key', 'trim|required');
	$this->form_validation->set_rules('status', 'status', 'trim|required');
	$this->form_validation->set_rules('created_datetime', 'created datetime', 'trim|required');
	$this->form_validation->set_rules('updated_datetime', 'updated datetime', 'trim|required');
	$this->form_validation->set_rules('created_by', 'created by', 'trim|required');
	$this->form_validation->set_rules('updated_by', 'updated by', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "transactions.xls";
        $judul = "transactions";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Total Price");
	xlsWriteLabel($tablehead, $kolomhead++, "Total Discount");
	xlsWriteLabel($tablehead, $kolomhead++, "Total Markup");
	xlsWriteLabel($tablehead, $kolomhead++, "Account Id");
	xlsWriteLabel($tablehead, $kolomhead++, "Customer Title");
	xlsWriteLabel($tablehead, $kolomhead++, "Customer Fullname");
	xlsWriteLabel($tablehead, $kolomhead++, "Customer Email");
	xlsWriteLabel($tablehead, $kolomhead++, "Customer Phone");
	xlsWriteLabel($tablehead, $kolomhead++, "Customer Address");
	xlsWriteLabel($tablehead, $kolomhead++, "Customer Tracker Key");
	xlsWriteLabel($tablehead, $kolomhead++, "Status");
	xlsWriteLabel($tablehead, $kolomhead++, "Created Datetime");
	xlsWriteLabel($tablehead, $kolomhead++, "Updated Datetime");
	xlsWriteLabel($tablehead, $kolomhead++, "Created By");
	xlsWriteLabel($tablehead, $kolomhead++, "Updated By");

	foreach ($this->Transactions_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->total_price);
	    xlsWriteNumber($tablebody, $kolombody++, $data->total_discount);
	    xlsWriteNumber($tablebody, $kolombody++, $data->total_markup);
	    xlsWriteNumber($tablebody, $kolombody++, $data->account_id);
	    xlsWriteLabel($tablebody, $kolombody++, $data->customer_title);
	    xlsWriteLabel($tablebody, $kolombody++, $data->customer_fullname);
	    xlsWriteLabel($tablebody, $kolombody++, $data->customer_email);
	    xlsWriteLabel($tablebody, $kolombody++, $data->customer_phone);
	    xlsWriteLabel($tablebody, $kolombody++, $data->customer_address);
	    xlsWriteLabel($tablebody, $kolombody++, $data->customer_tracker_key);
	    xlsWriteLabel($tablebody, $kolombody++, $data->status);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created_datetime);
	    xlsWriteLabel($tablebody, $kolombody++, $data->updated_datetime);
	    xlsWriteNumber($tablebody, $kolombody++, $data->created_by);
	    xlsWriteNumber($tablebody, $kolombody++, $data->updated_by);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=transactions.doc");

        $data = array(
            'transactions_data' => $this->Transactions_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('transactions/transactions_doc',$data);
    }

}

/* End of file Transactions.php */
/* Location: ./application/controllers/Transactions.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-11-22 11:05:45 */
/* http://harviacode.com */
